﻿using Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Networking;

namespace ServerApplication
{
    class GameStateThreadSafeWrapper
    {
        private GameStateRepository gameState;
        private object accessLock;

        internal Date date { get { return gameState.date; } }

        internal GameStateThreadSafeWrapper()
        {
            gameState = new GameStateRepository();
            accessLock = new object();
        }

        internal GameStateThreadSafeWrapper(GameStateRepository gameState)
        {
            this.gameState = gameState;
        }

        internal void advanceTimeOneMonth()
        {
            lock (accessLock)
            {
                gameState.advanceTimeOneMonth();
            }
        }

        internal void addRandomEstate()
        {
            lock (accessLock)
            {
                gameState.addRandomEstate();
            }
        }

        internal void addPlayer(string username)
        {
            lock (accessLock)
            {
                gameState.addPlayer(username);
            }
        }

        internal byte[] GameStateAsByteArray(GameDataSerializer gameDataSerializer)
        {
            byte[] data;
            lock (accessLock)
            {
                data = gameDataSerializer.objectToByteArray(gameState);
            }
            return data;
        }

        internal void addNewRandomPerson(PlayerRepository player)
        {
            lock (accessLock)
            {
                gameState.addNewRandomPerson(player);
            }
        }

        internal void addNewRandomPerson(string playerName)
        {
            lock (accessLock)
            {
                gameState.addNewRandomPerson(gameState.getPlayer(playerName));
            }
        }
    }
}
