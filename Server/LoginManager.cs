﻿using System;
using System.Collections.Generic;
using ClientApplication;
using Networking;

namespace ServerApplication
{
    internal class LoginManager
    {
        private Dictionary<string, string> unsafeEvilUsernamePasswordList;
        private Dictionary<ClientID, string> loggedInUsers;
        private object refreshLock;

        internal LoginManager()
        {
            unsafeEvilUsernamePasswordList = new Dictionary<string, string>();
            loggedInUsers = new Dictionary<ClientID, string>();
            refreshLock = new object();
        }

        internal bool tryLogIn(ClientID clientID, LoginRequest loginRequest)
        {
            string username = loginRequest.Username;
            string password = loginRequest.unsafePassword;

            lock (refreshLock)
            {
                if (unsafeEvilUsernamePasswordList.ContainsKey(username))
                {
                    if (!unsafeEvilUsernamePasswordList[username].Equals(password))
                    {
                        return false;
                    }              
                }
                else
                {
                    unsafeEvilUsernamePasswordList.Add(username, password);
                }
                if (loggedInUsers.ContainsKey(clientID))
                {
                    loggedInUsers[clientID] = username;
                }
                else
                {
                    //This should normally not happen - but might because of lag
                    return false;
                }
                return true;
            }            
        }

        internal void RefreshConnections(List<ClientID> currentConnections)
        {
            var expiredConnections = new List<ClientID>();
            var newConnections = new List<ClientID>();
            lock (refreshLock)
            {
                foreach (ClientID cid in loggedInUsers.Keys)
                {
                    if (!currentConnections.Contains(cid))
                    {
                        expiredConnections.Add(cid);
                    }
                }
                foreach (ClientID cid in currentConnections)
                {
                    if (!loggedInUsers.ContainsKey(cid))
                    {
                        newConnections.Add(cid);
                    }
                }
                foreach (ClientID cid in expiredConnections)
                {
                    loggedInUsers.Remove(cid);
                }
                foreach (ClientID cid in newConnections)
                {
                    loggedInUsers.Add(cid, null);
                }
            }
        }

        internal string getUsername(ClientID clientID)
        {
            return loggedInUsers[clientID];
        }
    }
}