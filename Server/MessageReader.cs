﻿using ClientApplication;
using Persons;
using System;
using Networking;

namespace ServerApplication
{
    class MessageReader
    {
        private SynchronousSocketListener server;
        private GameStateThreadSafeWrapper gameStateWrapper;
        private GameDataSerializer gameDataSerializer;
        private LoginManager loginManager;

        public MessageReader()
        {
            gameDataSerializer = new GameDataSerializer();
            server = new SynchronousSocketListener(9369, 1000);
            server.Initialize();
            initializeData();
            loginManager = new LoginManager();
            server.DataReceived += Server_DataReceived;
            server.ClientConnectionsRefreshed += Server_ClientConnectionsRefreshed;
        }

        private void Server_ClientConnectionsRefreshed(object sender, ClientConnectionsRefreshedEventArgs e)
        {
            loginManager.RefreshConnections(e.currentConnections);
        }

        private void Server_DataReceived(object sender, DataReceivedEventArgs e)
        {
            handleMessage(e.data, e.clientID);
        }

        private void sendMessages()
        {
            byte[] data = gameStateWrapper.GameStateAsByteArray(gameDataSerializer);
            server.SendToAll(data);
        }

        private void handleMessage(byte[] message, ClientID clientID)
        {
            object obj = null; // new object();
            try
            {
                obj = gameDataSerializer.ByteArrayToObject(message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            Console.WriteLine(obj.GetType() + " received");
            var name = obj as string;
            if (name != null)
            {
                handleStringMessage(name);
                return;
            }
            var loginRequest = obj as LoginRequest;
            if (loginRequest != null)
            {
                bool loginApproved = loginManager.tryLogIn(clientID, loginRequest);
                if (loginApproved)
                {
                    gameStateWrapper.addPlayer(loginRequest.Username);
                    sendToClient(clientID, string.Format("Welcome to the game, {0}!", loginRequest.Username));
                }
                else
                {
                    sendToClient(clientID, "Wrong password");                    
                }
                return;
            }
            var newPlayerCharacterRequest = obj as NewPlayerCharacterRequest;

            if (newPlayerCharacterRequest != null &&
                loginManager.getUsername(clientID).Equals(newPlayerCharacterRequest.PlayerName))
            {
                gameStateWrapper.addNewRandomPerson(newPlayerCharacterRequest.PlayerName);
                return;
            }
            Console.WriteLine("Could not process a message of type " + obj.GetType());
        }

        private void handleStringMessage(string name)
        {
           // var json_serializer = new JavaScriptSerializer();
        }

        private void sendToClient(ClientID clientID, string messageToShow)
        {
            byte[] messageToClient = gameDataSerializer.objectToByteArray(messageToShow);
            server.SendToSpecific(messageToClient, clientID);
        }

        private void initializeData()
        {
            gameStateWrapper = new GameStateThreadSafeWrapper();
            for (int i = 0; i < 5; i++)
            {
                gameStateWrapper.advanceTimeOneMonth();
                gameStateWrapper.addRandomEstate();
            }
            Console.WriteLine(gameStateWrapper.date.Year);
        }
        public static void Main()
        {
            MessageReader mr = new MessageReader();
            while (true)
            {
                string s = Console.ReadKey().KeyChar.ToString();
                Console.WriteLine(s);
                if (s.Equals("s"))
                {
                    Console.WriteLine("Sending GameState");
                    mr.sendMessages();
                }
                if (s.Equals("e"))
                {
                    Console.WriteLine("Elapsing GameState");
                    mr.gameStateWrapper.advanceTimeOneMonth();
                }
            }
        }
    }
}
