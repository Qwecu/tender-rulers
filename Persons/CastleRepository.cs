﻿using System;
using System.Collections.Generic;

namespace Persons
{
    /// <summary>
    /// Keeps track of Persons living in the same place
    /// </summary>
    [Serializable]
    internal class CastleRepository
    {
        private List<PersonRepository> residents;
        public CastleRepository()
        {
            residents = new List<PersonRepository>();
        }
        /// <summary>
        /// Movesa a Person from one castle to another
        /// </summary>
        /// <param name="p">The Person to move</param>
        /// <param name="from">The Castle to move from</param>
        /// <param name="to">The Castle to move to</param>
        internal static void move(PersonRepository p, CastleRepository from, CastleRepository to)
        {
            from.removeFromResidents(p);
            to.addToResidents(p);
        }

        /// <summary>
        /// Adds a Person to the Castle and sets the Person's Castle to this. Throws Exception if the Person's Castle field is not null.
        /// </summary>
        /// <param name="p">The Person to add</param>
        internal void addToResidents(PersonRepository p)
        {
            if (p.castle != null)
            {
                throw new InvalidOperationException("Tried to add a Person to a Castle but their Castle field was already assigned");
            }
            residents.Add(p);
            p.castle = this;
        }
        /// <summary>
        /// Removes a Person from the Castle. Throws Exception if the Person is not found there
        /// </summary>
        /// <param name="p">The Person to remove</param>
        internal void removeFromResidents(PersonRepository p)
        {
            if (p.castle == this)
            {
                p.castle = null;
            }
            else
            {
                throw new InvalidOperationException("Tried to remove a person from the castle but they were not there to begin with");
            }
            residents.Remove(p);
        }
        internal IList<PersonRepository> getResidents()
        {
            return residents.AsReadOnly();
        }
        /// <summary>
        /// Removes all dead Persons from the Castle. The Persons keep their Castle field intact.
        /// </summary>
        internal void removeDead()
        {
            var dead = new List<PersonRepository>();
            foreach (PersonRepository p in residents)
            {
                if (p.isLiving == false)
                {
                    dead.Add(p);
                }
            }
            foreach (PersonRepository p in dead)
            {
                residents.Remove(p);
            }
        }
    }
}