﻿using System;

namespace Persons
{
    [Serializable]
    public class MarriageProposalRepository
    {
        public readonly int dowry;
        public readonly PersonRepository female;
        public readonly PersonRepository male;
        public bool expired { get; private set; }

        public MarriageProposalRepository(PersonRepository male, PersonRepository female, int dowry)
        {
            this.male = male;
            this.female = female;
            this.dowry = dowry;
            expired = false;
        }
        internal void reject()
        {
            expired = true;
        }
    }
}