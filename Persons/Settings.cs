﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderRulers;

namespace Persons
{
    static internal class Settings
    {
            internal static Random random;
        //    private static int maxPopulation;
        //    internal static int minDynasties;
        //    internal static double workNeededPerCompetition;
        //    internal static int messageBufferSize { get; private set; }
        //    internal static int maxPCsPerPlayer;
        //    internal static readonly int MinMarryingAge;
        //    internal static int PlayerMessageListLength;

           static Settings()
            {
            random = GlobalSettings.random;
        //        maxPopulation = 200; //not absolutely strict
        //        minDynasties = 20;
        //        workNeededPerCompetition = 100.0;
        //        messageBufferSize = 100;
        //        maxPCsPerPlayer = 5;
        //        MinMarryingAge = 15;
        //        PlayerMessageListLength = 100;
          }

        //    /// <summary>
        //    /// this sets the minimum limits of dynasty glory to get the council chairs available
        //    /// </summary>
        //    internal static double[] gloryLimits = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };


        //    internal static double randomEstateSize()
        //    {
        //        return random.NextDouble() + random.NextDouble() * 100;
        //    }

        //    internal static double randomEstateWealth()
        //    {
        //        return random.NextDouble() + random.NextDouble();
        //    }

        //    internal static bool willIConceive(Person person, Procreator procreator, int populationCount)
        //    {
        //        if (person.gender != Gender.female) return false;
        //        if (populationCount > maxPopulation) return false;
        //        double fertility = procreator.getFertility();
        //        double i = random.NextDouble();
        //        if (i < fertility) return true;
        //        return false;
        //    }

        //    internal static double monthlyGloryIncreaseFromOnePersonToTheirDynasty(double gloryOfMember)
        //    {
        //        return gloryOfMember / 200;
        //    }

        //    internal static Tuple<int, int> randomMaxAgeYearsMonths()
        //    {
        //        int monthsToLive = (int)NextGaussian(600.0, 200.0);
        //        int yearsToLive = monthsToLive / 12;
        //        return Tuple.Create(yearsToLive, monthsToLive % 12);
        //    }

        //    internal static double NextGaussian(double mu = 0, double sigma = 1)
        //    {
        //        var u1 = random.NextDouble();
        //        var u2 = random.NextDouble();

        //        var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
        //                            Math.Sin(2.0 * Math.PI * u2);

        //        var randNormal = mu + sigma * randStdNormal;

        //        return randNormal;
        //    }

        //    internal static int yearlyTaxFromDynastyMemberToTheirDynasty(Dynasty dynasty, int personalWealth)
        //    {
        //        Person steward = dynasty.getCouncilMember(DynastyCouncilChair.Steward);
        //        if (steward == null) return 0;
        //        double stewardSkill = steward.getSkill(SkillName.Moneymaking);
        //        int tax = (int)(stewardSkill / 100 * personalWealth);
        //        return tax;
        //    }

        //    internal static double DecreaseDynastyGloryMonthly()
        //    {
        //        return 0.99;
        //    }

        //    internal static double DecreasePersonGloryMonthly => 0.99;
        //}
        ////Enum members can be added freely between builds
        //public enum SkillName
        //{
        //    Moneymaking, Charisma, Organizing, Strength, Agility, Creativity
        //}

        //public enum Gender
        //{
        //    female, male
        //}

        //public enum DynastyCouncilChair
        //{
        //    Dictator, Warrior, Steward, Diplomat, MC, Architect
        //}
    }
}
