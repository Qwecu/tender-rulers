﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderEntities;
using TenderRulers;

namespace Persons
{
    /// <summary>
    /// A Competition
    /// </summary>
    [Serializable]
    public class CompetitionRepository
    {
        //public Person Holder { get; private set; }
        //public string Name { get; private set; }
        //public int PrizeMoney { get; private set; }
        //public HashSet<Person> Contestants;
        //public Date TimeOfHappeningYearsMonths { get; private set; }
        //public int Year { get { return TimeOfHappeningYearsMonths.Year; } }
        //public int Month { get { return TimeOfHappeningYearsMonths.Month; } }
        //internal bool Executed { get; private set; }
        //internal Person Winner { get; private set; }

        //public int SignUpCount
        //{
        //    get { return Contestants.Count; }
        //}
        //private Dictionary<SkillName, int> requirements;
        //private int luck;

        private static MessengerRepository messenger;

        ///// <summary>
        ///// A new Competition
        ///// </summary>
        ///// <param name="holder">Organizer of the Competition</param>
        ///// <param name="prize">Prize money dealt out</param>
        ///// <param name="when">Date of the Competition</param>
        ///// <param name="requirements"></param>
        ///// <param name="luckValue">A value between 0-200, at 0 the result is determined straight from skill level, at 100 randomized between 0 and skill level, at 200 totally randomized</param>
        //internal Competition(Person holder, int prize, Date when, Dictionary<SkillName, int> requirements = null, int luckValue = 0, string name = "random")
        //{
        //    if (luckValue < 0 || luckValue > 200)
        //    {
        //        throw new ArgumentException("luckValue must be between 0 and 200");
        //    }
        //    this.Holder = holder;
        //    PrizeMoney = prize;
        //    Contestants = new HashSet<Person>();
        //    TimeOfHappeningYearsMonths = when;
        //    this.Name = name;
        //    //this.requirements = requirements;
        //    Executed = false;
        //    luck = luckValue;
        //    messenger.addMessage(string.Format("{0} will be holding a {1} contest next year", holder.Name, name));
        //}

        internal static void setMessenger(MessengerRepository m)
        {
            messenger = m;
        }

        /// <summary>
        /// Adds a Person to the Competition. Duplicates are not added.
        /// </summary>
        /// <param name="p">The Person to add</param>
        //internal void addContestant(Person p)
        //{
        //    Contestants.Add(p);
        //}

        //public IList<Person> getContestants()
        //{
        //    return (IList<Person>)Contestants;
        //}

        ///// <summary>
        ///// Executes the Competition if its time of happening matches with the given Date
        ///// </summary>
        ///// <param name="currentTime">The given Date</param>
        //internal void tryExecute(Date currentTime)
        //{
        //    if (TimeOfHappeningYearsMonths == currentTime)
        //    {
        //        execute();
        //    }
        //}

        /// <summary>
        /// Makes a skill check on every living contestant and gives the prize money to the Person with the highest result
        /// </summary>
        internal static Tuple<int, double> Execute(List<TenderEntities.Person> competitors, TenderEntities.Competition competition, TenderEntities.Person Organizer, TenderContext dc)
        {
            //messenger.addMessage("Executing contest by " + Organizer.Name);
            var results = countResults(competition, competitors, dc);
            if (results.Count > 0)
            {
                var keys = results.Keys.ToArray();
                Array.Reverse(keys);
                double winningResult = (double)keys[0];
                TenderEntities.Person Winner = results[winningResult];
               // messenger.addMessage("The winner is " + Winner + " with the result " + Math.Round(winningResult, 2));
                Winner.Wealth += competition.PrizeMoney;
                competition.Executed = true;
                competition.WinnerId = Winner.PersonId;
                competition.WinningResult = winningResult;
                dc.SaveChanges();
                return Tuple.Create(Winner.PersonId, winningResult);
            }
            else
            {
                return null;
            }
            //else
            //{
            //    messenger.addMessage("There were no contestants");
            //    Organizer.Wealth += competition.PrizeMoney;
            //}
            
        }

        /// <summary>
        /// Makes a skill check on ever contestant and adds the results to the Dictionary.
        /// </summary>
        /// <param name="results"></param>
        private static SortedDictionary<double?, TenderEntities.Person> countResults(TenderEntities.Competition competition, List<TenderEntities.Person> competitors, TenderContext dc)
        {
            var results = new SortedDictionary<double?, TenderEntities.Person>();
            double sumOfWeights = 0.0;
            var requirements = dc.CompetitionSkillRequirements.Where(x => x.CompetitionId == competition.Id).ToList();
            sumOfWeights = requirements.Sum(x => x.Weight);            
            
            foreach (var person in competitors)
            {
                if (person.IsLiving())
                {
                    double result = countResult(competition, person, requirements, sumOfWeights, dc);
                    try
                    {
                        results.Add(result, person);
                    }
                    //For cases where the result is exactly the same
                    catch (ArgumentException e)
                    {
                        while (true)
                        {
                            try
                            {
                                results.Add((result + (Settings.random.NextDouble() - 0.5) / 100), person);
                                break;
                            }
                            catch (ArgumentException e2)
                            {
                            }
                        }
                    }
                }
            }
            return results;
        }



        /// <summary>
        /// Counts the skill check for an individual
        /// </summary>
        /// <param name="person">The Person to check on</param>
        /// <param name="sumOfSkillWeights">Sum of skill requirement weight values</param>
        /// <returns></returns>
        private static double countResult(TenderEntities.Competition competition, TenderEntities.Person person, List<CompetitionSkillRequirement> requirements, double sumOfSkillWeights, TenderContext dc)
        {
            double totalResult = 0.0;
            Random random = Settings.random;
            if (requirements == null)
            {
                return totalResult;
            }
            else if (competition.Luck <= 100)
            {
                
                foreach (var requirement in requirements)
                {
                    double skillResult = 0;
                    skillResult += random.NextDouble() * 2 * PersonRepository.GetSkill(person.PersonId, requirement.SkillId) * (competition.Luck * 1.0 / 100);
                    skillResult += PersonRepository.GetSkill(person.PersonId, requirement.SkillId) * ((100 - competition.Luck) * 1.0 / 100);
                    skillResult *= requirement.Weight / sumOfSkillWeights;
                    totalResult += skillResult;
                }
            }
            else
            {
                int luck2 = competition.Luck - 100;
                foreach (var requirement in requirements)
                {
                    double skillResult = 0;
                    skillResult += random.NextDouble() * 2 * PersonRepository.GetSkill(person.PersonId, requirement.SkillId) * ((100 - luck2) / 100);
                    skillResult += random.NextDouble() * 15 * (luck2 * 1.0 / 100);
                    skillResult *= requirement.Weight / sumOfSkillWeights;
                    totalResult += skillResult;
                }
            }
            return totalResult;
        }
    }
}
