﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderRulers;
using TenderEntities;

namespace Persons
{
    public class PersonRepository
    {
        // private NameObject nameObj;
        public DynastyRepository dynasty { get; private set; }
        internal CastleRepository castle { get; set; }
        private SkillsetRepository skillset;
        public int wealth { get; private set; }
        public Tuple<int, int> ageYearsMonths { get; private set; }
        public Date? birthdayYearsMonths { get; private set; }
        private Tuple<int, int> maxAgeYearsMonths;
        private List<EstateRepository> estates;

        public static List<Person> GetLivingPersonsOfPlayer(int _playerId)
        {
            using (var dc = new TenderContext())
            {
                return dc.Persons.Where(x => x.PlayerId == _playerId && x.Born && !x.Dead).ToList();
            }
        }

        public static List<PersonListItem> GetLivingPersonsOfPlayerListItem(int _playerId)
        {
            using (var dc = new TenderContext())
            {
                return dc.Persons.Where(x => x.PlayerId == _playerId && x.Born && !x.Dead).Select(x => new PersonListItem()
                {
                    Name = x.Name,
                    DynastyName = x.FK_Dynasty.Name,
                    Glory = (int)x.Glory,
                    Wealth = x.Wealth,
                    Age = x.AgeInMonths / 12,
                    Castle = x.PlaceOfResidenceId.ToString()
                }).ToList();
            }
        }

        public class PersonListItem
        {
            public string Name { get; set; }
            public string DynastyName { get; set; }
            public int Age { get; set; }
            public int Glory { get; set; }
            public int Wealth { get; set; }
            public string Castle { get; set; }
        }

        internal bool isLiving { get; set; }
        internal readonly Gender gender;
        public string genderString { get { return gender.ToString(); } }
        public double glory { get; private set; }
        public PlayerRepository Player { get; private set; }
        private Procreator procreator;
        private MarriageRepository marriage;



        internal static void AddNewRandomDynasty(TenderContext dc, Player player = null)
        {
            var dynasty = EntityFactory.RandomDynasty();
            dc.Dynasties.Add(dynasty);
            dc.SaveChanges();
            var person = EntityFactory.RandomPerson();
            var name = EntityFactory.RandomNameObject();
            person.DynastyId = dynasty.Id;
            person.BirthTime = dc.getGameState().Time - GlobalSettings.RandomAgeInMonthsNewPerson;
            person.AgeInMonths = dc.getGameState().Time - person.BirthTime.Value;
            person.DeathTime = person.BirthTime + GlobalSettings.RandomDyingAgeInMonths();
            person.Born = true;
            dc.Persons.Add(person);
            dc.NameObjects.Add(name);
            dc.SaveChanges();
            name.PersonId = person.PersonId;
            //person.NameObjectId = name.Id;
            person.Name = name.Name;
            var castle = new TenderEntities.Castle();
            dc.Castles.Add(castle);
            dc.SaveChanges();
            dynasty.CastleId = castle.Id;
            person.PlaceOfResidenceId = castle.Id;
            dc.SaveChanges();
            if (player != null)
            {
                dynasty.PlayerId = player.Id;
                person.PlayerId = player.Id;
            }
            dc.SaveChanges();
        }

        public static TenderEntities.Person addNewRandomPerson()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;

                TenderEntities.Person p = new TenderEntities.Person();
                TenderEntities.Dynasty d = new TenderEntities.Dynasty();
                p.DynastyId = d.Id;
                p.BirthTime = date.InMonths();
                p.DeathTime = p.BirthTime.Value + GlobalSettings.RandomDyingAgeInMonths();
                dc.Dynasties.Add(d);
                dc.Persons.Add(p);
                dc.SaveChanges();
                return p;
            }

        }

        public static void AddNewRandomPerson(int _playerId)
        {
            Player player;
            using (var dc = new TenderContext())
            {
                player = dc.Players.Single(x => x.Id == _playerId);
                AddNewRandomDynasty(dc, player);

            }
        }

        /// <summary>
        /// Adds a new random person with a new dynasty and adds the dynasty to the player.
        /// </summary>
        /// <param name="player">The player of the new person</param>
        /// <returns>The new person</returns>
        //public static void AddNewRandomPerson(TenderEntities.Player player)
        //{
        //    if (player == null)
        //    {
        //        return;
        //    }

        //    using (var dc = new TenderContext())
        //    {
        //        TenderEntities.Dynasty dynasty = EntityFactory.RandomDynasty();
        //        dynasty.PlayerId = player.Id;
        //        dc.Dynasties.Add(dynasty);
        //        dc.SaveChanges();
        //        TenderEntities.Person newbie = EntityFactory.RandomPerson();
        //        newbie.DynastyId = dynasty.Id;
        //        newbie.PlayerId = player.Id;
        //        newbie.Born = true;
        //        dc.Persons.Add(newbie);
        //        dc.SaveChanges();
        //    }
        //}

        //internal Marriage.MaritalStatus maritalStatus
        //{
        //    get { return marriage == null ? Marriage.MaritalStatus.unmarried : marriage.maritalStatus(this); }
        //}
        //public string MaritalStatusString { get { return maritalStatus.ToString(); } }
        public List<MarriageProposalRepository> MarriageProposals { get; private set; }
        //  internal bool isPregnant { get { return procreator.monthsPregnant == null ? false : true; } }
        private static int chromosomeLength;
        private byte[,] genes;
        public PersonRepository Mother { get; internal set; }
        public PersonRepository Father { get; internal set; }
        public HashSet<PersonRepository> Children { get; private set; }

        static PersonRepository()
        {
            chromosomeLength = 64;
        }
        private PersonRepository()
        {
            castle = null;
            estates = new List<EstateRepository>();
            wealth = 0;
            ageYearsMonths = Tuple.Create(0, 0);
            //birthdayYearsMonths = new Date();
            maxAgeYearsMonths = GlobalSettings.randomMaxAgeYearsMonths();
            isLiving = true;
            gender = Settings.random.Next(2) == 0 ? Gender.female : Gender.male;
            glory = 0.0;
            Player = null;
            // procreator = new Procreator(this, gender);
            marriage = null;
            MarriageProposals = new List<MarriageProposalRepository>();
            Children = new HashSet<PersonRepository>();
        }

        //public Person(Date? currentTimeYearMonth, bool isNewborn, Dynasty dynasty = null, Person mother = null, Person father = null)
        //    : this()
        //{
        //    if (isNewborn)
        //    {
        //        birthdayYearsMonths = currentTimeYearMonth;
        //    }
        //    else
        //    {
        //        throw new NotImplementedException("Currently only newborn people can be created");
        //    }
        //    this.dynasty = dynasty;
        //    if (this.dynasty == null)
        //        this.dynasty = new Dynasty();

        //    if (mother == null && father == null)
        //    {
        //        nameObj = new NameObject();
        //        //skillset = new Skillset(isNewborn);
        //        genes = randomiseGenes();
        //    }
        //    else if (mother != null && father != null)
        //    {
        //        nameObj = new NameObject(mother.nameObj, father.nameObj);
        //        //skillset = new Skillset(mother.skillset, father.skillset, isNewborn);
        //        genes = mixGenes(mother.genes, father.genes);
        //    }
        //    else if (father == null)
        //    {
        //        nameObj = new NameObject(mother.nameObj);
        //        //skillset = new Skillset(mother.skillset, isNewborn);
        //        genes = mixGenes(mother.genes, randomiseGenes());
        //    }
        //    else if (mother == null)
        //    {
        //        throw new ArgumentException("Fathers cannot currently make babies alone");
        //    }
        //}

        private byte[,] mixGenes(byte[,] genes1, byte[,] genes2)
        {
            var newGenes = new byte[2, chromosomeLength];
            Random random = Settings.random;
            for (int i = 0; i < chromosomeLength; i++)
            {
                byte motherGene;
                byte fatherGene;

                int r = random.Next(2);
                motherGene = r == 0 ? genes1[0, i] : genes1[1, i];
                r = random.Next(2);
                fatherGene = r == 0 ? genes2[0, i] : genes2[1, i];
                r = random.Next(2);
                if (r == 0)
                {
                    newGenes[0, i] = motherGene;
                    newGenes[1, i] = fatherGene;
                }
                else
                {
                    newGenes[0, i] = fatherGene;
                    newGenes[1, i] = motherGene;
                }
            }
            return newGenes;
        }

        ///// <summary>
        ///// If the person is of age, this returns their best marriage proposal
        ///// </summary>
        ///// <returns>The best proposal or null</returns>
        //internal static MarriageProposal tryAcceptProposal(TenderEntities.Person person, Date now)
        //{
        //    if (person.Age(now).Value.Year < GlobalSettings.MinMarryingAge)
        //    {
        //        return null;
        //    }
        //    MarriageProposal accepted = null;
        //    foreach (MarriageProposal mp in MarriageProposals)
        //    {
        //        if (accepted == null)
        //        {
        //            accepted = mp;
        //        }
        //        else if (accepted.dowry < mp.dowry)
        //        {
        //            accepted = mp;
        //        }
        //    }
        //    return accepted;
        //}

        private byte[,] randomiseGenes()
        {
            var newGenes = new byte[2, chromosomeLength];

            var chromosome1 = new byte[chromosomeLength];
            var chromosome2 = new byte[chromosomeLength];

            Settings.random.NextBytes(chromosome1);
            Settings.random.NextBytes(chromosome2);

            for (int i = 0; i < chromosomeLength; i++)
            {
                newGenes[0, i] = chromosome1[i];
                newGenes[1, i] = chromosome2[i];
            }
            return newGenes;
        }

        internal byte getGene(int index)
        {
            return genes[0, index];
        }

        /// <summary>
        /// A sum of dowry to offer in marriage, more generous with a lesser moneymaking skill
        /// </summary>
        /// <returns>The dowry</returns>
        internal static int dowry(TenderEntities.Person person, double moneySkill)
        {
            //double moneySkill = skillset.getSkill(SkillName.Moneymaking);
            if (moneySkill == 0.0)
            {
                return person.Wealth;
            }
            return (int)Math.Min(person.Wealth * 1.0 / moneySkill, person.Wealth);
        }

        internal void addMarriageProposal(MarriageProposalRepository proposal)
        {
            MarriageProposals.Add(proposal);
        }

        internal void removeExpiredMarriageProposals()
        {
            var old = new List<MarriageProposalRepository>();
            foreach (MarriageProposalRepository mp in MarriageProposals)
            {
                if (mp.expired)
                {
                    old.Add(mp);
                }
            }
            foreach (MarriageProposalRepository mp in old)
            {
                MarriageProposals.Remove(mp);
            }
        }

        /// <summary>
        /// Sets the player of the person, no checking of a previous player
        /// </summary>
        /// <param name="player">The new player</param>
        public void appointToPlayer(PlayerRepository player)
        {
            this.Player = player;
        }

        //internal static bool tryToMakeBabies(int popCount)
        //{
        //    bool isLucky = GlobalSettings.willIConceive(this.gender, procreator.getFertility(), popCount);
        //    return isLucky;
        //}


        internal double getSkill(SkillName skillName)
        {
            return skillset.getSkill(skillName);
        }

        /// <summary>
        /// Increases the wealth by w. No checking for negative values
        /// </summary>
        /// <param name="w">The wealth to be added</param>
        internal void increaseWealth(int w)
        {
            wealth += w;
        }

        internal void addEstate(EstateRepository estate)
        {
            estates.Add(estate);
        }

        public static void agePersonOneMonth(TenderEntities.Person person)
        {
            //increaseAgeMonthly(person);
            person.AgeInMonths++;
            increaseGloryMonthly(person);

        }

        private static void increaseGloryMonthly(TenderEntities.Person person)
        {
            person.Glory = (person.Glory + GetSkill(person.PersonId, (int)SkillName.Charisma)) * GlobalSettings.DecreasePersonGloryMonthly;
        }

        public static double GetSkill(int personId, int skillId)
        {
            using (var dc = new TenderContext())
            {
                var skill = dc.PersonalSkills.SingleOrDefault(x => x.FK_Person.PersonId == personId && x.SkillId == skillId);
                if (skill != null) return skill.SkillValue;
                var newSkill = EntityFactory.RandomPersonalSkill();
                newSkill.PersonId = personId;
                newSkill.SkillId = skillId;
                int age = dc.Persons.Single(x => x.PersonId == personId).AgeInMonths;
                SkillsetRepository.CountSkill(dc, newSkill, age);
                dc.PersonalSkills.Add(newSkill);
                dc.SaveChanges();
                return newSkill.SkillValue;
            }
        }
        public static PersonalSkill GetSkillObject(int personId, int skillId)
        {
            using (var dc = new TenderContext())
            {
                var skill = dc.PersonalSkills.SingleOrDefault(x => x.FK_Person.PersonId == personId && x.SkillId == skillId);
                if (skill != null) return skill;
                var newSkill = EntityFactory.RandomPersonalSkill();
                newSkill.PersonId = personId;
                newSkill.SkillId = skillId;
                int age = dc.Persons.Single(x => x.PersonId == personId).AgeInMonths;
                SkillsetRepository.CountSkill(dc, newSkill, age);
                dc.PersonalSkills.Add(newSkill);
                dc.SaveChanges();
                return newSkill;
            }
        }

        public static Gene GetGene(int personId, int geneTypeId)
        {
            using (var dc = new TenderContext())
            {
                var gene = dc.Genes.SingleOrDefault(x => x.PersonId == personId && x.GeneTypeId == geneTypeId);
                if (gene == null)
                {
                    gene = EntityFactory.RandomGene(personId, geneTypeId);
                    dc.Genes.Add(gene);
                    dc.SaveChanges();
                }
                return gene;
            }
        }


        //public override string ToString()
        //{
        //    return (Name + " " + DynastyString);
        //}

        //public string description
        //{
        //    get { return (nameObj + " " + skillset); }
        //}
        //public string Name
        //{
        //    get { return nameObj.ToString(); }
        //}
        public string DynastyString
        {
            get { return dynasty.ToString(); }
        }
        public string Skills
        {
            get { return skillset.SkillListString; }
        }
        public int Wealth
        {
            get { return wealth; }
        }
        public string AgeString
        {
            get
            {
                string yearString = (ageYearsMonths.Item1 == 1) ? "year" : "years";
                string monthString = (ageYearsMonths.Item2 == 1) ? "month" : "months";
                return string.Format("{0} {1}, {2} {3}", ageYearsMonths.Item1, yearString, ageYearsMonths.Item2, monthString);
            }
        }

        internal List<EstateRepository> Estates
        {
            get { return estates; }
        }

        public string EstatesString
        {
            get
            {
                if (estates.Count == 0) return "none";
                StringBuilder sb = new StringBuilder();
                double sum = 0;
                foreach (EstateRepository e in estates)
                {
                    sb.AppendLine(e.ToString());
                    sum += e.size;
                }
                sb.Remove(sb.Length - 2, 2);
                return "Total : " + (int)sum + "km²\n" + sb.ToString();
            }
        }

        public bool timeToDie
        {
            get
            {
                if (ageYearsMonths.Item1 < maxAgeYearsMonths.Item1)
                    return false;
                if (ageYearsMonths.Item2 > maxAgeYearsMonths.Item2 || ageYearsMonths.Item1 > maxAgeYearsMonths.Item1)
                    return true;
                return false;
            }
        }

        //internal bool marry(Person spouse)
        //{
        //    if (this.maritalStatus == Marriage.MaritalStatus.married || spouse.maritalStatus == Marriage.MaritalStatus.married)
        //    {
        //        return false;
        //    }
        //    this.endMarriage(); //in case of widowhood
        //    spouse.endMarriage();
        //    Marriage newMarriage = new Marriage(this, spouse);
        //    this.marriage = newMarriage;
        //    spouse.marriage = newMarriage;
        //    return true;
        //}

        internal void setAllMarriageProposalsToExpired()
        {
            foreach (MarriageProposalRepository mp in MarriageProposals)
            {
                mp.reject();
            }
        }

        //internal void endMarriage()
        //{
        //    if (marriage != null)
        //    {
        //        marriage.leaveMarriage(this);
        //        marriage = null;
        //    }
        //}

        //internal PersonRepository elapsePregnancy()
        //{
        //    return procreator.elapsePregnancy();
        //}

        internal void conceiveBaby()
        {
            //if (gender != Gender.female)
            //{
            //    throw new InvalidOperationException("Only females can get pregnant");
            //}
            //if (marriage == null)
            //{
            //    procreator.conceiveBaby(null);
            //}
            //else
            //{
            //    procreator.conceiveBaby(marriage.getSpouse(this));
            //}
        }

        //public Person Spouse()
        //{
        //    if (marriage == null) return null;
        //    return marriage.getSpouse(this);
        //}

        internal void setBirthday(Date yearMonth)
        {
            birthdayYearsMonths = yearMonth;
        }
    }
}
