﻿using System;
using System.Collections.Generic;
using TenderRulers;

namespace Persons
{
    [Serializable]
    internal class PlayerMessageManager
    {
        private Dictionary<PlayerRepository, List<PlayerMessage>> playerMessages;
        public PlayerMessageManager()
        {
            playerMessages = new Dictionary<PlayerRepository, List<PlayerMessage>>();
        }
        internal void addPlayerMessage(PlayerRepository player, PlayerMessageType messageType, string message, PersonRepository relevantPerson = null)
        {
            if (!playerMessages.ContainsKey(player))
            {
                playerMessages.Add(player, new List<PlayerMessage>());
            }
            var messageList = playerMessages[player];
            messageList.Add(new PlayerMessage(messageType, message, relevantPerson));
            while (messageList.Count > GlobalSettings.PlayerMessageListLength)
            {
                messageList.RemoveAt(0);
            }
        }
        internal List<PlayerMessage> getMessages(PlayerRepository player)
        {
            if (!playerMessages.ContainsKey(player))
            {
                return null;
            }
            return playerMessages[player];
        }
    }
}