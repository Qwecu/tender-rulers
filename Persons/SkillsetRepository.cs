﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderEntities;
using TenderRulers;

namespace Persons
{
    /// <summary>
    /// Attaches personal values to skill names
    /// </summary>
    [Serializable]
    internal class SkillsetRepository
    {
        /// <summary>
        /// The skillset phenotype, visible in-game
        /// </summary>
        private double[] skills;
        private double[] skillChromosome1;
        private double[] skillChromosome2;

        private static readonly int amountOfSkills;
        private static Random random = GlobalSettings.random;

        static SkillsetRepository()
        {
            amountOfSkills = Enum.GetNames(typeof(SkillName)).Length;
        }

        ///// <summary>
        ///// Makes a new random skillset
        ///// </summary>
        //internal Skillset(bool isNewborn = false)
        //{
        //    skills = new double[amountOfSkills];
        //    skillChromosome1 = new double[amountOfSkills];
        //    skillChromosome2 = new double[amountOfSkills];

        //    for (int i = 0; i < amountOfSkills; i++)
        //    {
        //        skillChromosome1[i] = random.NextDouble() * 20;
        //        skillChromosome2[i] = random.NextDouble() * 20;
        //    }
        //    if (!isNewborn)
        //    {
        //        countSkills();
        //    } //else all is 0   
        //}

        ///// <summary>
        ///// Returns a new skillset, inheriting half of the genes from the mother and half from an anonymous father
        ///// </summary>
        ///// <param name="mother">The mother skillset</param>
        //internal Skillset(Skillset mother, bool isNewborn = false)
        //{
        //    Skillset father = new Skillset();
        //    makeChild(mother, father, isNewborn);
        //}

        /// <summary>
        /// Returns a new skillset, inheriting half of the genes from the mother and half from the father
        /// </summary>
        /// <param name="mother">The mother skillset</param>
        /// <param name="father">The father skillset</param>
        //internal Skillset(Skillset mother, Skillset father, bool isNewborn = false)
        //{
        //    makeChild(mother, father, isNewborn);
        //}

        ///// <summary>
        ///// Mixes the mother and father genes
        ///// </summary>
        ///// <param name="mother">The mother skillset</param>
        ///// <param name="father">The father skillset</param>
        //private void makeChild(Skillset mother, Skillset father, bool isNewborn)
        //{
        //    skills = new double[amountOfSkills];
        //    skillChromosome1 = new double[amountOfSkills];
        //    skillChromosome2 = new double[amountOfSkills];

        //    for (int i = 0; i < amountOfSkills; i++)
        //    {
        //        double motherGene = random.Next(2) == 0 ? mother.skillChromosome1[i] : mother.skillChromosome2[i];
        //        double fatherGene = random.Next(2) == 0 ? father.skillChromosome1[i] : father.skillChromosome2[i];

        //        int dominant = random.Next(2);
        //        if (dominant == 0)
        //        {
        //            skillChromosome1[i] = motherGene;
        //            skillChromosome2[i] = fatherGene;
        //        }
        //        else
        //        {
        //            skillChromosome1[i] = fatherGene;
        //            skillChromosome2[i] = motherGene;
        //        }
        //        if (!isNewborn)
        //        {
        //            countSkills();
        //        } //else all is 0   
        //    }
        //}

        /// <summary>
        /// Updates the skillset phenotype to represent an adult
        /// </summary>
        internal static void CountSkills(TenderContext dc, List<PersonalSkill> skills, int ageInMonths)
        {
            foreach (PersonalSkill skill in skills)
            {
                CountSkill(dc, skill, ageInMonths);
            }
        }

        internal static void CountSkill(TenderContext dc, PersonalSkill skill, int ageInMonths)
        {
            if (ageInMonths > 250)
            {
                skill.SkillValue = (skill.Gene1 + skill.Gene2) / 2;
            }
            else
            {
                skill.SkillValue = skill.SkillPotential * (ageInMonths * 1.0 / 250);
            }
        }

        ///// <summary>
        ///// Updates the skillset phenotype to represent the specified age
        ///// </summary>
        //public static void countSkills(List<PersonalSkill> skills)
        //{
        //    foreach(var skill in skills) {
               
        //            countSkills();
               
        //    }
            
        //    else
        //    {
        //        for (int i = 0; i < amountOfSkills; i++)
        //        {
        //            var baseSkill = (skillChromosome1[i] + skillChromosome2[i]) / 2;
        //            skills[i] = baseSkill * (ageInMonths * 1.0 / 250);
        //        }
        //    }
        //}

        internal double getSkill(SkillName skillName)
        {
            return skills[(int)skillName];
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Skills:");
            for (int i = 0; i < amountOfSkills; i++)
            {
                sb.Append(" " + Enum.GetName(typeof(SkillName), i) + ": ");
                sb.Append((int)skills[i]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// A string containing skill names and values
        /// </summary>
        public string SkillListString
        {
            get
            {
                var sb = new StringBuilder();
                for (int i = 0; i < amountOfSkills; i++)
                {
                    sb.Append(Enum.GetName(typeof(SkillName), i));
                    sb.Append(": ");
                    sb.Append((int)skills[i]);
                    if (i < amountOfSkills - 1)
                    {
                        sb.Append("\n");
                    }
                }
                return sb.ToString();
            }
        }
    }
}
