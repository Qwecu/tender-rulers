﻿using System;
using System.Collections.Generic;
using TenderEntities;
using TenderRulers;
using System.Linq;

namespace Persons
{
    /// <summary>
    /// A Dynasty is a collective of related People. It has a Council and owns money
    /// </summary>
    [Serializable]
    public class DynastyRepository
    {
        //public string name { get; private set; }
        //public override string ToString() { return name; }
        //private List<Person> members; //all members, living and dead
        //private double gloryUnroundedValue;
        //public double glory { get { return Math.Round(gloryUnroundedValue, 2); } }
        //public int wealth { get; private set; }
        //public int memberWealthSum { get { return wealthMembersCombined(); } }
        //private double workLeftBeforeNextCompetitionCanBePlanned;

        private static MessengerRepository messenger;

        private int wealthMembersCombined(TenderEntities.Dynasty dynasty, TenderContext dc)
        {
            int combinedWealth = (from person in dc.Persons
                                  where person.Born && !person.Dead
                                  && person.DynastyId == dynasty.Id
                                  group person by dynasty.Id into persons
                                  select persons.Sum(x => x.Wealth)).Single();

            return combinedWealth;
        }

        private CastleRepository castle;
        private DynastyCouncilRepository council;
        //public int memberAmount { get { return amountOfLivingMembers(); } }
        public PlayerRepository player { get; private set; }

        //public Dynasty()
        //{
        //    name = NameObject.randomNameString();
        //    members = new List<Person>();
        //    council = new DynastyCouncil();
        //    castle = new Castle();
        //    gloryUnroundedValue = 0.0;
        //    workLeftBeforeNextCompetitionCanBePlanned = GlobalSettings.workNeededPerCompetition;
        //    wealth = 0;
        //}

        internal static void setMessenger(MessengerRepository m)
        {
            messenger = m;
        }

        internal void setPlayer(PlayerRepository p)
        {
            player = p;
        }

        public double getGlory(TenderEntities.Dynasty dynasty)
        {
            return Math.Round(dynasty.GloryUnroundedValue, 2);
        }

        internal int amountOfLivingMembers(TenderEntities.Dynasty dynasty, TenderContext dc)
        {
            return (from person in dc.Persons
                    where person.IsLiving()
                    select person).Count();
        }

        public IReadOnlyList<TenderEntities.Person> livingPeople(TenderEntities.Dynasty dynasty, TenderContext dc)
        {
            return (from person in dc.Persons
                    where person.Born && !person.Dead
                    select person).ToList();
        }

        internal IList<TenderEntities.Person> castleResidents(TenderEntities.Dynasty dynasty, TenderContext dc)
        {
            List<TenderEntities.Person> pers = new List<TenderEntities.Person>();
            if (!dynasty.CastleId.HasValue)
            {
                return pers;
            }
            return (from person in dc.Persons
                    where person.Born && !person.Dead
                    && person.PlaceOfResidenceId.HasValue
                    && person.PlaceOfResidenceId == dynasty.CastleId.Value
                    select person).ToList();
        }

        public List<object> getVerbalInfo(TenderEntities.Dynasty dynasty, TenderContext dc)
        {
            List<object> info = new List<object>();
            info.Add(dynasty.Name);
            info.Add("Glory: " + Math.Round(dynasty.GloryUnroundedValue, 2));
            info.Add("Council Members:");
            foreach (DynastyCouncilChair chair in Enum.GetValues(typeof(DynastyCouncilChair)))
            {
                PersonRepository chairHolder = council.getChairHolder(chair);
              //  string name = chairHolder == null ? "none" : (chairHolder.Name + " " + chairHolder.dynasty);
              //  info.Add(chair + ": " + name);
            }
            info.Add("Living members:");
            foreach (var p in livingPeople(dynasty, dc))
            {
                info.Add(p.Name);
            }
            info.Add("Residing at the home castle:");
            foreach (PersonRepository p in castle.getResidents())
            {
             //   info.Add(string.Format("{0} {1}", p.Name, p.DynastyString));
            }
            return info;
        }

        //internal void doYearlyTasks()
        //{
        //    collectTaxesFromDynastyMembers();
        //}

        /// <summary>
        /// Checks whether enough work has been done to host a new Competition and returns a new one if so.
        /// </summary>
        /// <param name="currentDate">The current Date, used for determining the Date of the new Competition</param>
        /// <returns>The new Competition, returns null if needs more organizing</returns>
        internal static TenderEntities.Competition considerHostingANewCompetition(TenderContext dc, Date currentDate, TenderEntities.Dynasty dynasty, TenderEntities.Person organizer, double organizingSkill, double moneymakingSkill, byte luckFactor)
        {
            TenderEntities.Competition competition = null;
            if (organizer == null) return competition;
            dynasty.WorkLeftBeforeNextCompetitionCanBePlanned -= organizingSkill;
            if (dynasty.WorkLeftBeforeNextCompetitionCanBePlanned < 0)
            {
                dynasty.WorkLeftBeforeNextCompetitionCanBePlanned += GlobalSettings.workNeededPerCompetition;

                //The prize money is based on Organizer's Moneymaking skill (better skill, less money dealt out)
                int prize = Math.Min(dynasty.Wealth, (int)(dynasty.Wealth * 1.0 / moneymakingSkill));
                dynasty.Wealth -= prize;

                var skillsMeasured = new Dictionary<SkillName, int>();
                skillsMeasured.Add(SkillName.Organizing, (int)organizingSkill);

                Date when = new Date(currentDate.Year + 1, currentDate.Month);

                int luck = Math.Max(0, luckFactor - 55); //a value between 0 and 200

                string name = string.Format("{0} {1} and be the {2} {3}", (organizer.WordVerb.Word.Substring(0, 1).ToUpper() + organizer.WordVerb.Word.Substring(1)), organizer.WordAdverb.Word, organizer.WordAdjective.Word, organizer.WordNoun.Word);
                MessengerRepository.AddMessage(dc, string.Format("{0}! {1} dynasty offers {2} prize money out of their treasury of {3}", name, dynasty.Name, prize, dynasty.Wealth), currentDate.InMonths());

                competition = new TenderEntities.Competition()
                {
                    HolderId = organizer.PersonId,
                    Name = name,
                    PrizeMoney = prize,
                    YearMonthOfOccurrence = when.InMonths(),
                    Executed = false,
                    Luck = luck
                };
            }
            return competition;
        }

        ///// <summary>
        ///// Collects a money tax from all the Dynasty members tho the Dynasty
        ///// </summary>
        //private void collectTaxesFromDynastyMembers()
        //{
        //    //foreach(Person p in livingPeople())
        //    //{
        //    //    int tax = GlobalSettings.yearlyTaxFromDynastyMemberToTheirDynasty(this, p.wealth);
        //    //    p.increaseWealth(-tax);
        //    //    wealth += tax;
        //    //}
        //}

        public PersonRepository[] getCouncilMembers()
        {
            return council.getCouncilMembers();
        }

        public PersonRepository getCouncilMember(DynastyCouncilChair chair)
        {
            return council.getChairHolder(chair);
        }

        //internal void doMonthlyTasks()
        //{
        //    checkForDeadPeople();
        //    // getGloryFromMembers();
        //    //UpdateCouncil();
        //}

        //private void checkForDeadPeople()
        //{
        //    castle.removeDead();
        //}

        //private void getGloryFromMembers()
        //{
        //    foreach (Person p in members)
        //    {
        //        if (p.isLiving == true)
        //        {
        //            gloryUnroundedValue += GlobalSettings.monthlyGloryIncreaseFromOnePersonToTheirDynasty(p.glory);
        //        }
        //    }
        //}

        internal static void UpdateCouncils(TenderContext dc)
        {
            var time = dc.getGameState().Time;
            foreach (var dynasty in dc.Dynasties.ToList())
            {

                var currentCouncilMemberships = (from chair in dc.CouncilMemberships
                                                 join person in dc.Persons on chair.DynastyId equals person.PersonId
                                                 where chair.DynastyId == dynasty.Id
                                                 select chair);
                List<TenderEntities.CouncilMembership> personsToRemove = new List<CouncilMembership>();
                if (!dynasty.CastleId.HasValue)
                {
                    personsToRemove = currentCouncilMemberships.ToList();
                }
                else
                {
                    personsToRemove = (from member in dc.Persons
                                       join chair in currentCouncilMemberships on member.PersonId equals chair.PersonId
                                       where (member.Born && !member.Dead) == false ||
                                       !member.PlaceOfResidenceId.HasValue ||
                                       member.PlaceOfResidenceId != dynasty.CastleId.Value
                                       select chair).ToList();
                }
                dc.CouncilMemberships.RemoveRange(personsToRemove);

                var emptyChairIds = new List<int>();
                var chairIds = Enum.GetValues(typeof(DynastyCouncilChair)).Cast<int>().ToList();
                emptyChairIds = (from chair in chairIds
                                 where GlobalSettings.gloryLimits[(int)chair] <= dynasty.GloryUnroundedValue &&
                                 !dc.CouncilMemberships.Any(x => x.ChairId == chair && x.DynastyId == dynasty.Id) &&
                                 dynasty.CastleId.HasValue
                                 select (int)chair).ToList();

                foreach (var chairId in emptyChairIds)//(DynastyCouncilChair chair in Enum.GetValues(typeof(DynastyCouncilChair)))
                {
                    //Eligible for council chair & chair is empty && dynasty has a castle
                    //if (GlobalSettings.gloryLimits[(int)chair] >= dynasty.GloryUnroundedValue &&
                    //    dc.CouncilMemberships.SingleOrDefault(x => x.ChairId == (int)chair && x.DynastyId == dynasty.Id) == null &&
                    //    dynasty.CastleId.HasValue)
                    //{
                    var candidates = (from person in dc.Persons
                                      where person.DynastyId == dynasty.Id
                                      && person.Born && !person.Dead
                                      && person.PlaceOfResidenceId.HasValue
                                      && person.PlaceOfResidenceId == dynasty.CastleId.Value
                                      && !dc.CouncilMemberships.Any(x => x.PersonId == person.PersonId)
                                      select person).ToList();
                    if (candidates.Count > 0)
                    {
                        var newMember = candidates[Settings.random.Next(candidates.Count())];
                        dc.CouncilMemberships.Add(new CouncilMembership()
                        {
                            ChairId = chairId,
                            DynastyId = dynasty.Id,
                            PersonId = newMember.PersonId
                        });
                        MessengerRepository.AddMessage(dc, string.Format("The {0} dynasty appointed {1} for {2}", dynasty.Name, newMember.FullName, (DynastyCouncilChair)chairId), time);
                    }
                    dc.SaveChanges();
                }
            }
        }
    }
}
