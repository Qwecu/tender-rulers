﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderEntities;
using TenderRulers;

namespace Persons
{
    [Serializable]
    internal class MessengerRepository
    {
        //private List<string> messages;
        ///// <summary>
        ///// The amount of messages saved
        ///// </summary>
        //private int messageBufferSize;

        //public Messenger()
        //{
        //    messages = new List<string>();
        //    messageBufferSize = GlobalSettings.messageBufferSize;
        //}
        /// <summary>
        /// Adds a string to the global Message list.
        /// </summary>
        /// <param name="message">The string to add</param>
        internal static void AddMessage(TenderContext dc, string message, int time)
        {
            dc.PublicMessages.Add(new PublicMessage()
            {
                Time = time,
                Message = message
            });
            Console.WriteLine(message);
        }
    }
}
