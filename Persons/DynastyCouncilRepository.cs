﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderRulers;

namespace Persons
{
    [Serializable]
    class DynastyCouncilRepository
    {
        private PersonRepository[] chairHolders;

        private static MessengerRepository messenger;

        public DynastyCouncilRepository()
        {
            int amoutOfChairs = Enum.GetNames(typeof(DynastyCouncilChair)).Length;
            chairHolders = new PersonRepository[amoutOfChairs];
        }
        public PersonRepository getChairHolder(DynastyCouncilChair chair)
        {
            return chairHolders[(int)chair];
        }
        public void setChairHolder(DynastyCouncilChair chair, PersonRepository p)
        {
            if (!p.isLiving)
            {
                throw new ArgumentException("Dead people cannot be appointed to council chairs");
            }
            chairHolders[(int)chair] = p;
        }

        internal static void setMessenger(MessengerRepository m)
        {
            messenger = m;
        }

        internal PersonRepository[] getCouncilMembers()
        {
            return chairHolders;
        }

        internal int getChairIndexOf(PersonRepository p)
        {
            if (!chairHolders.Contains(p))
            {
                return -1;
            }
            return Array.IndexOf(chairHolders, p);
        }
    }
}