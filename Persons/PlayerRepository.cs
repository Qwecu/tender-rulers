﻿using System;
using System.Linq;
using TenderEntities;

namespace Persons
{
    public class PlayerRepository
    {
        static PlayerRepository()
        {
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
        }
        public static TenderEntities.Player GetPlayer(string userId)
        {
            using (var dc = new TenderContext())
            {
                System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
                var player = dc.Players.SingleOrDefault(x => x.UserId == userId);
                if (player == null)
                {
                    player = new TenderEntities.Player()
                    {
                        UserId = userId
                    };
                    dc.Players.Add(player);
                    dc.SaveChanges();
                }
                    return player;
            }
        }

        public static TenderEntities.Player GetPlayer(int playerId)
        {
            using (var dc = new TenderContext())
            {
                System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
                var player = dc.Players.SingleOrDefault(x => x.Id == playerId);
                return player;
            }
        }

        public static void UpdatePlayerName(int _playerId, string playerName)
        {
            using (var dc = new TenderContext())
            {
                dc.Players.Single(x => x.Id == _playerId).Name = playerName;
                dc.SaveChanges();
            }
        }
    }
}