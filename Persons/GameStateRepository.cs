﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderEntities;
using TenderRulers;

namespace Persons
{
    [Serializable]
    public class GameStateRepository
    {

        private static Random random;
        static GameStateRepository()
        {
            random = GlobalSettings.random;
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
        }

        //public IList<Person> getLivingPeople(Player activePlayer)
        //{
        //    return activePlayer.getLivingPersons();
        //}

        //public IList<Person> personsOf(Player player)
        //{
        //    return player.getLivingPersons();
        //}

        //public IList<Dynasty> dynastiesOf(Player player)
        //{
        //    return player.getDynasties();
        //}

        public IList<TenderEntities.Dynasty> getDynasties()
        {
            using (var dc = new TenderContext())
            {
                return dc.Dynasties.ToList().AsReadOnly();
            }
        }

        public IList<TenderEntities.Estate> getEstates()
        {
            using (var dc = new TenderContext())
            {
                return dc.Estates.ToList().AsReadOnly();
            }
        }

        public IList<TenderEntities.Competition> getUpcomingCompetitions()
        {
            using (var dc = new TenderContext())
            {
                return (from comp in dc.Competitions
                        where comp.YearMonthOfOccurrence > dc.getGameState().Time
                        select comp).ToList();
            }
        }

        public void advanceTimeOneMonth()
        {
            using (var dc = new TenderContext())
            {

                advanceWorldDate();
                Date date = dc.getGameState().Date;
                ageEveryLivingPersonOneMonth();
                killOldPeople();
                conceiveAndBirthBabies();
                updateDynastiesMonthly();
                estatesMakeProfit();

                makeMarriageProposals();
                marryPeople();
                removeExpiredProposals();

                executeCompetitions();
                planNewCompetitions();
                signUpWarriorsForCompetitions();

                if (date.Month == 1)
                {
                    updateDynastiesYearly();
                    addRandomEstate();
                }
                MessengerRepository.AddMessage(dc, string.Format("Year {0}, month {1}", date.Year, date.Month), date.InMonths());
            }
        }

        /// <summary>
        /// Checks the NPC females' received marriage proposals and marries couples according to them
        /// </summary>
        private void marryPeople()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;
                List<TenderEntities.MarriageProposal> acceptedProposals = new List<TenderEntities.MarriageProposal>();
                foreach (TenderEntities.Person p in dc.Persons.Where(x => x.Born && !x.Dead))
                {
                    //the person must be a non-married npc female
                    if (p.GenderId == (int)Gender.male || p.PlayerId.HasValue || p.AgeInMonths / 12 < GlobalSettings.MinMarryingAge)
                    {
                        continue;
                    }
                    var best = dc.MarriageProposals.Where(x => x.ProposeeId == p.PersonId).OrderByDescending(x => x.Dowry).FirstOrDefault();
                    if (best != null) acceptedProposals.Add(best);


                }
                while (acceptedProposals.Count > 0)
                {
                    TenderEntities.MarriageProposal toBeMarried = acceptedProposals[random.Next(acceptedProposals.Count)];
                    int fiance = toBeMarried.ProposerId;
                    int fiancee = toBeMarried.ProposeeId;
                    int dowry = toBeMarried.Dowry;
                    if (dc.Marriages.Where(x => (x.HusbandId == fiance || x.WifeId == fiancee) && x.Active).Count() == 0)
                    {
                        var marriage = new TenderEntities.Marriage()
                        {
                            HusbandId = fiance,
                            WifeId = fiancee,
                            MarriageYear = date.Year,
                            MarriageMonth = date.Month,
                            Active = true
                        };
                        dc.Marriages.Add(marriage);
                        var husband = dc.Persons.Single(x => x.PersonId == fiance);
                        var wife = dc.Persons.Single(x => x.PersonId == fiancee);
                        husband.Wealth -= (dowry);
                        wife.Wealth += (dowry);
                        MessengerRepository.AddMessage(dc, string.Format("{0} married {1} and paid her {2} in dowry", husband.FullName, wife.FullName, dowry), date.InMonths());
                        foreach (TenderEntities.MarriageProposal proposal in dc.MarriageProposals.Where(x => x.ProposerId == fiance || x.ProposeeId == fiancee))
                        {
                            proposal.Active = false;
                        }
                    }
                    acceptedProposals.Remove(toBeMarried);
                    dc.SaveChanges();
                }
                dc.SaveChanges();
            }
        }

        ///// <summary>
        ///// If the person is of age, this returns their best marriage proposal
        ///// </summary>
        ///// <returns>The best proposal or null</returns>
        //internal static TenderEntities.MarriageProposal TryAcceptProposal(TenderEntities.Person person, Date now)
        //{
        //    if (person.Age(now).Value.Year < GlobalSettings.MinMarryingAge)
        //    {
        //        return null;
        //    }
        //    MarriageProposal accepted = null;
        //    foreach (MarriageProposal mp in MarriageProposals)
        //    {
        //        if (accepted == null)
        //        {
        //            accepted = mp;
        //        }
        //        else if (accepted.dowry < mp.dowry)
        //        {
        //            accepted = mp;
        //        }
        //    }
        //    return accepted;
        //}

        public bool addMarriageProposal(int maleId, int femaleId, int dowry)
        {
            using (var dc = new TenderContext())
            {
                var male = dc.Persons.SingleOrDefault(x => x.PersonId == maleId && x.GenderId == (int)Gender.male);
                var female = dc.Persons.SingleOrDefault(x => x.PersonId == femaleId && x.GenderId == (int)Gender.female);
                if (male == null || female == null || !male.IsLiving() || !(female.IsLiving()))
                {
                    return false;
                }
                if (dc.Marriages.Any(x => x.HusbandId == maleId && x.Active || x.WifeId == femaleId && x.Active))
                {
                    return false;
                }
                dc.MarriageProposals.Add(new TenderEntities.MarriageProposal()
                {
                    ProposerId = maleId,
                    ProposeeId = femaleId,
                    Dowry = dowry
                });
                dc.SaveChanges();
            }
            return true;
        }

        /// <summary>
        /// Removes expired proposals from every person's personal proposal list
        /// </summary>
        private void removeExpiredProposals()
        {
            using (var dc = new TenderContext())
            {
                var expired = from proposal in dc.MarriageProposals
                              join male in dc.Persons on proposal.ProposerId equals male.PersonId
                              join female in dc.Persons on proposal.ProposeeId equals female.PersonId
                              join marriageF in dc.Marriages on female.PersonId equals marriageF.WifeId
                              join marriageM in dc.Marriages on male.PersonId equals marriageM.HusbandId
                              where marriageF.Active || marriageM.Active
                              select proposal.Id;

                dc.MarriageProposals.RemoveRange(dc.MarriageProposals.Where(x => expired.Contains(x.Id)));
                dc.SaveChanges();
            }
        }

        private void makeMarriageProposals()
        {
            using (var dc = new TenderContext())
            {
                var unmarriedNPCMales = dc.Persons.Where(x => x.GenderId == (int)Gender.male && x.PlayerId == null && x.Born && !x.Dead && dc.Marriages.FirstOrDefault(y => y.HusbandId == x.PersonId && y.Active) == null).ToList();
                var females = dc.Persons.Where(x => x.Born && !x.Dead && x.GenderId == (int)Gender.female).ToList();
                if (females.Count == 0) return;
                foreach (var male in unmarriedNPCMales)
                {
                    var female = females[random.Next(females.Count)];
                    if (!dc.Marriages.Any(x => x.WifeId == female.PersonId && x.Active))
                    {
                        int dowry = PersonRepository.dowry(male, PersonRepository.GetSkill(male.PersonId, (int)SkillName.Moneymaking));
                        var proposal = new TenderEntities.MarriageProposal() { Active = true, Dowry = dowry, ProposerId = male.PersonId, ProposeeId = female.PersonId };
                        dc.MarriageProposals.Add(proposal);
                        if (female.PlayerId != null)
                        {
                            string message = string.Format("{0} received a marriage proposal from {1}", female.FullName, male.FullName);
                            dc.PlayerMessages.Add(new TenderEntities.PlayerMessage()
                            { Header = "Proposal received", PlayerId = female.PlayerId.Value, TypeId = (int)PlayerMessageType.ReceivedProposal, Message = message });
                            //playerMessageManager.addPlayerMessage(female.PlayerId, PlayerMessageType.ReceivedProposal, message, female);
                        }
                    }
                }
                dc.SaveChanges();
            }
        }

        private void signUpWarriorsForCompetitions()
        {
            using (var dc = new TenderContext())
            {
                var newSignUps = new List<CompetitionSignUp>();
                var currentTime = dc.getGameState().Time;
                var NPCDynastyWarriors = (from dynasty in dc.Dynasties
                                          join person in dc.Persons on dynasty.Id equals person.DynastyId
                                          join chair in dc.CouncilMemberships on person.PersonId equals chair.PersonId
                                          where chair.ChairId == (int)DynastyCouncilChair.Warrior &&
                                          dynasty.PlayerId == null
                                          select person).ToList();
                var upcomingCompetitions = (from competition in dc.Competitions
                                            where competition.YearMonthOfOccurrence > currentTime && competition.Executed == false
                                            select competition).ToList();

                if (upcomingCompetitions.Count == 0) return;

                foreach (var warrior in NPCDynastyWarriors)
                {
                    var comp = upcomingCompetitions[random.Next(upcomingCompetitions.Count())];
                    //the warrior must not be occupied at the given time
                    if (!dc.CompetitionSignUps.Any(x => x.PersonId == warrior.PersonId && x.FK_Competition.YearMonthOfOccurrence == comp.YearMonthOfOccurrence))
                    {
                        newSignUps.Add(new CompetitionSignUp()
                        {
                            CompetitionId = comp.Id,
                            PersonId = warrior.PersonId
                        });
                    }
                }
                dc.CompetitionSignUps.AddRange(newSignUps);
                dc.SaveChanges();
            }
        }

        private void updateDynastiesYearly()
        {
            using (var dc = new TenderContext())
            {
                foreach (var dynasty in dc.Dynasties)
                {
                    var steward = dc.CouncilMemberships.SingleOrDefault(x => x.DynastyId == dynasty.Id && x.ChairId == (int)DynastyCouncilChair.Steward);
                    double stewardSkill = 0;
                    if (steward != null)
                    {
                        stewardSkill = PersonRepository.GetSkill(steward.PersonId, (int)SkillName.Moneymaking);
                    }
                    foreach (var person in dc.Persons.Where(x => x.Born && !x.Dead && x.DynastyId == dynasty.Id))
                    {
                        int tax = GlobalSettings.yearlyTaxFromDynastyMemberToTheirDynasty(stewardSkill, person.Wealth);
                        person.Wealth -= tax;
                        dynasty.Wealth += tax;
                    }
                }
                dc.SaveChanges();
            }
        }

        private void updateDynastiesMonthly()
        {
            using (var dc = new TenderContext())
            {
                DynastyRepository.UpdateCouncils(dc);

                double factor = GlobalSettings.MonthlyGloryIncreaseFromOnePersonToTheirDynasty;
                var gloryIncreases = (from person in dc.Persons
                                      where person.Born && !person.Dead
                                      join dynasty in dc.Dynasties
                                      on person.DynastyId equals dynasty.Id
                                      group person by dynasty.Id into dynGroup
                                      select new
                                      {
                                          DynId = dynGroup.Key,
                                          Amount = dynGroup.Sum(x => x.Glory)
                                      }).ToList();
                foreach (var increase in gloryIncreases)
                {
                    dc.Dynasties.Single(x => x.Id == increase.DynId).GloryUnroundedValue += increase.Amount;
                }
                foreach (var dynasty in dc.Dynasties)
                {
                    dynasty.GloryUnroundedValue *= GlobalSettings.DecreaseDynastyGloryMonthly;
                }
                dc.SaveChanges();

                int dynastyCount = (from dynasty in dc.Dynasties
                                    join person in dc.Persons on dynasty.Id equals person.DynastyId
                                    where person.Born && !person.Dead
                                    group dynasty by dynasty.Id into ongoingDynasties
                                    select ongoingDynasties).Count();
                if (dynastyCount < GlobalSettings.minDynasties)
                {
                    PersonRepository.AddNewRandomDynasty(dc);
                }
            }
        }



        public void addPlayer(string name)
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;
                foreach (var player in dc.Players)
                {
                    if (player.Name.Equals(name))
                    {
                        MessengerRepository.AddMessage(dc, string.Format("The player name {0} already exists", name), date.InMonths());
                        dc.SaveChanges();
                        return;
                    }
                }
                dc.Players.Add(new TenderEntities.Player()
                {
                    Name = name,
                    Username = name
                });
                MessengerRepository.AddMessage(dc, string.Format("A new player, {0}, joined the game", name), date.InMonths());
                dc.SaveChanges();
            }
        }

        public TenderEntities.Player getPlayer(string name)
        {
            using (var dc = new TenderContext())
            {
                return dc.Players.SingleOrDefault(x => x.Name.Equals(name));
            }
        }

        private void planNewCompetitions()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;
                foreach (TenderEntities.Dynasty d in dc.Dynasties.ToList())
                {
                    TenderEntities.Person MC = (from chairs in dc.CouncilMemberships
                                                join person in dc.Persons on chairs.PersonId equals person.PersonId
                                                where chairs.DynastyId == d.Id
                                                && chairs.ChairId == (int)DynastyCouncilChair.MC
                                                select person).SingleOrDefault();
                    if (MC != null)
                    {
                        double organizing = PersonRepository.GetSkill(MC.PersonId, (int)SkillName.Organizing);
                        TenderEntities.Competition competition = DynastyRepository.considerHostingANewCompetition(dc, date, d, MC, PersonRepository.GetSkill(MC.PersonId, (int)SkillName.Organizing), PersonRepository.GetSkill(MC.PersonId, (int)SkillName.Moneymaking), PersonRepository.GetGene(MC.PersonId, (int)GeneType.LuckFactorInCompetitions).GeneValue);
                        if (competition != null)
                        {
                            dc.Competitions.Add(competition);
                        }
                        dc.SaveChanges();
                    }
                }
            }
        }

        private void executeCompetitions()
        {
            using (var dc = new TenderContext())
            {
                var time = dc.getGameState().Time;
                //var executedCompetitions = new List<Competition>();
                foreach (var c in dc.Competitions.ToList())
                {
                    if (c.YearMonthOfOccurrence == time)
                    {
                        MessengerRepository.AddMessage(dc, "Executing contest by " + c.FK_Holder.FullName, time);
                        var contestants = (from signup in dc.CompetitionSignUps
                                           join person in dc.Persons on signup.PersonId equals person.PersonId
                                           where signup.CompetitionId == c.Id
                                           && person.Born && !person.Dead
                                           select person).ToList();
                        if (contestants.Count() > 0)
                        {
                            var winnerAndResult = CompetitionRepository.Execute(contestants, c, dc.Persons.Single(x => x.PersonId == c.HolderId), dc);
                            if (winnerAndResult != null)
                            {
                                var winner = dc.Persons.Single(x => x.PersonId == winnerAndResult.Item1);
                                MessengerRepository.AddMessage(dc, "The winner is " + winner.FullName + " with the result " + Math.Round(winnerAndResult.Item2, 2), time);
                                if (winner.FK_Player != null)
                                {
                                    string message = string.Format("{0} won a competition with a prize of {1}!", winner.FullName, c.PrizeMoney.ToString());
                                    dc.PlayerMessages.Add(new TenderEntities.PlayerMessage()
                                    {
                                        Header = "You won a competition! Yippee!",
                                        PlayerId = winner.PlayerId.Value,
                                        Message = message,
                                        TypeId = (int)PlayerMessageType.CompetitionWon
                                    });
                                }
                            }
                            else
                            {
                                MessengerRepository.AddMessage(dc, "There was no winner", time); //Should not happen
                                c.FK_Holder.Wealth += c.PrizeMoney;
                            }
                        }
                        else
                        {
                            MessengerRepository.AddMessage(dc, "There were no contestants", time);
                            c.FK_Holder.Wealth += c.PrizeMoney;
                        }
                    }
                }
                dc.SaveChanges();
            }
        }

        public void AddCompetition(PersonRepository holder, int prize, Date when, string name = "random", Dictionary<SkillName, int> requirements = null, int luckValue = 0)
        {

        }

        public IEnumerable<string> getMessages()
        {
            using (var dc = new TenderContext())
            {
                return dc.PublicMessages.Select(x => x.Message).ToList();
            }
        }

        private void killOldPeople()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;

                var dyingPeople = new List<TenderEntities.Person>();
                foreach (var p in dc.Persons.Where(x => x.Born && !x.Dead))
                {
                    if (p.DeathTime.HasValue)
                    {
                        if (p.DeathTime.Value <= date.InMonths()) dyingPeople.Add(p);
                    }

                }
                foreach (TenderEntities.Person dying in dyingPeople)
                {
                    string deathMessage = string.Format("{0} died of old age at the age of {1}", dying.FullName, dying.AgeInMonths / 12);
                    MessengerRepository.AddMessage(dc, deathMessage, date.InMonths());
                    if (dying.PlayerId != null)
                    {
                        dc.PlayerMessages.Add(new TenderEntities.PlayerMessage()
                        {
                            Header = "Death in the family",
                            Message = deathMessage,
                            PlayerId = dying.PlayerId.Value,
                            TypeId = (int)PlayerMessageType.PCDiedOfOldAge
                        });
                    }
                    dying.Dead = true;
                    divideInheritance(dying);
                    foreach (var x in dc.Marriages.Where(x => x.HusbandId == dying.PersonId || x.WifeId == dying.PersonId))
                    {
                        x.Active = false;
                    }
                    foreach (var x in dc.MarriageProposals.Where(x => x.ProposerId == dying.PersonId || x.ProposeeId == dying.PersonId))
                    {
                        x.Active = false;
                    }
                    var endingCouncilMemberships = dc.CouncilMemberships.Where(x => x.PersonId == dying.PersonId);
                    dc.CouncilMemberships.RemoveRange(dc.CouncilMemberships.Where(x => x.PersonId == dying.PersonId));
                    foreach (var ending in endingCouncilMemberships)
                    {
                        MessengerRepository.AddMessage(dc, (String.Format("{0} is gone from the castle and no longer the {1} of {2}", dying.FullName, (DynastyCouncilChair)ending.ChairId, dying.FK_Dynasty.Name)), date.InMonths());

                    }
                }
                dc.SaveChanges();

            }
        }

        private void divideInheritance(TenderEntities.Person deceased)
        {
            using (var dc = new TenderContext())
            {
                var inheritors = dc.Persons.Where(x => x.DynastyId.Value == deceased.DynastyId.Value && x.Born && !x.Dead).ToList();
                if (inheritors.Count() == 0) { inheritors = dc.Persons.Where(x => x.Born && !x.Dead).ToList(); }
                if (inheritors.Count() == 0) return;
                int moneyToInherit = deceased.Wealth / inheritors.Count();
                int extra = deceased.Wealth % inheritors.Count();
                foreach (var inheritor in inheritors)
                {
                    inheritor.Wealth += (moneyToInherit);
                }
                inheritors.First().Wealth += (extra);
                foreach (var estateOs in dc.EstateOwnerships.Where(x => x.OwnerId == deceased.PersonId))
                {
                    estateOs.OwnerId = inheritors[random.Next(inheritors.Count())].PersonId;
                }
                dc.SaveChanges();
            }
        }

        private void BirthBabies()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;
                var time = date.InMonths();
                List<PersonRepository> babies = new List<PersonRepository>();
                var endingPregnancies = dc.ParentChildRelationships.Where(x => x.Type == (int)ParentChildRelationshipType.UnbornInWomb && (x.StartingTime + 9) <= time);
                foreach (var pregnancy in endingPregnancies)
                {
                    var mother = new ParentChildRelationship()
                    {
                        ChildId = pregnancy.ChildId,
                        ParentId = pregnancy.ParentId,
                        Type = (int)ParentChildRelationshipType.Mother,
                        StartingTime = date.InMonths()
                    };
                    dc.ParentChildRelationships.Add(mother);
                    var parentMarriage = dc.Marriages.FirstOrDefault(y => y.WifeId == pregnancy.ParentId);
                    if (parentMarriage != null)
                    {
                        var father = new ParentChildRelationship()
                        {
                            ChildId = pregnancy.ChildId,
                            ParentId = parentMarriage.HusbandId,
                            Type = (int)ParentChildRelationshipType.Father,
                            StartingTime = date.InMonths()
                        };
                        dc.ParentChildRelationships.Add(father);
                    }
                    var child = dc.Persons.Where(x => x.PersonId == pregnancy.ChildId).Single();
                    child.Born = true;
                    child.BirthTime = date.InMonths();
                    child.DeathTime = child.BirthTime.Value + GlobalSettings.RandomDyingAgeInMonths();
                    child.FertilityModifier = Settings.random.NextDouble();
                    var fatherPerson = parentMarriage == null ? null : dc.Persons.SingleOrDefault(x => x.PersonId == parentMarriage.HusbandId);
                    var motherPerson = dc.Persons.Single(x => x.PersonId == pregnancy.ParentId);
                    child.DynastyId = fatherPerson?.DynastyId ?? motherPerson.DynastyId;
                    child.PlaceOfResidenceId = motherPerson.PlaceOfResidenceId;
                    var motherName = motherPerson != null ? dc.NameObjects.SingleOrDefault(x => x.PersonId == motherPerson.PersonId) : null;
                    var fatherName = fatherPerson != null ? dc.NameObjects.SingleOrDefault(x => x.PersonId == fatherPerson.PersonId) : null;
                    var childName = EntityFactory.RandomNameObject(motherName, fatherName);
                    childName.PersonId = child.PersonId;
                    dc.NameObjects.Add(childName);
                    //child.NameObjectId = childName.Id;
                    child.Name = childName.Name;
                    if (fatherPerson?.PlayerId != null && dc.Persons.Where(x => x.PlayerId.Value == fatherPerson.PlayerId.Value && x.Born && !x.Dead).Count() < GlobalSettings.maxPCsPerPlayer)
                    {
                        child.PlayerId = fatherPerson.PlayerId;
                    }
                    else if (motherPerson.PlayerId != null && dc.Persons.Where(x => x.PlayerId.Value == motherPerson.PlayerId.Value && x.Born && !x.Dead).Count() < GlobalSettings.maxPCsPerPlayer)
                    {
                        child.PlayerId = motherPerson.PlayerId;
                    }
                    MessengerRepository.AddMessage(dc, string.Format("{0} gave birth to a beautiful baby, {1}", motherPerson.FullName, child.FullName), date.InMonths());
                }
                dc.ParentChildRelationships.RemoveRange(endingPregnancies);
                dc.SaveChanges();
            }
        }

        private void ConceiveBabies()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;
                var time = date.InMonths();
                var potentialMothers = (from mother in dc.Persons
                                        where mother.Born == true
                                        && mother.Dead == false
                                        && mother.GenderId == (int)Gender.female
                                        && mother.AgeInMonths >= 15 * 12 //15 years
                                        && !dc.ParentChildRelationships.Any(x => x.ParentId == mother.PersonId && x.Type == (int)ParentChildRelationshipType.UnbornInWomb)
                                        select new { mother.PersonId, mother.FertilityModifier, mother.Name }).ToList();
                int belowCap = GlobalSettings.MaxPopulation - dc.Persons.Where(x => x.Born && !x.Dead).Count();
                int potentialMothersCount = potentialMothers.Count();
                int amountOfBabies = Math.Min(belowCap / 15, potentialMothersCount / 15);
                var theLuckyOnes = (from potentialMother in potentialMothers
                                    select new
                                    {
                                        potentialMother.PersonId,
                                        Luck = PersonRepository.GetSkill(potentialMother.PersonId, (int)SkillName.Fertility) * potentialMother.FertilityModifier * random.NextDouble(),
                                        Name = potentialMother.Name
                                    }).OrderByDescending(x => x.Luck).Take(amountOfBabies).ToList();
                var skillsToAdd = new List<PersonalSkill>();

                foreach (var lucky in theLuckyOnes)
                {
                    TenderEntities.Person baby = new TenderEntities.Person()
                    {
                        WordAdjectiveId = GlobalSettings.RandomAdjectiveId,
                        WordAdverbId = GlobalSettings.RandomAdverbId,
                        WordNounId = GlobalSettings.RandomNounId,
                        WordVerbId = GlobalSettings.RandomVerbId
                    };
                    var parentalMarriage = dc.Marriages.SingleOrDefault(x => x.Active && x.WifeId == lucky.PersonId);
                    int motherId = lucky.PersonId;
                    int fatherId = parentalMarriage?.HusbandId ?? 0;
                    dc.Persons.Add(baby);
                    dc.SaveChanges();
                    MessengerRepository.AddMessage(dc, string.Format("{0} is expecting a baby!", lucky.Name), time);
                    ParentChildRelationship mommy = new ParentChildRelationship()
                    {
                        ChildId = baby.PersonId,
                        ParentId = motherId,
                        Type = (int)ParentChildRelationshipType.UnbornInWomb,
                        StartingTime = time
                    };
                    ParentChildRelationship daddy = new ParentChildRelationship()
                    {
                        ChildId = baby.PersonId,
                        ParentId = fatherId,
                        Type = (int)ParentChildRelationshipType.GeneticFather,
                        StartingTime = time
                    };
                    dc.ParentChildRelationships.Add(mommy);
                    dc.ParentChildRelationships.Add(daddy);
                    dc.SaveChanges();

                    foreach (SkillName skillName in Enum.GetValues(typeof(SkillName)))
                    {
                        if (fatherId != 0)
                        {
                            skillsToAdd.Add(EntityFactory.InheritedSkill(PersonRepository.GetSkillObject(motherId, (int)skillName), PersonRepository.GetSkillObject(fatherId, (int)skillName), baby.PersonId));
                        }
                        else
                        {
                            skillsToAdd.Add(EntityFactory.InheritedSkill(PersonRepository.GetSkillObject(motherId, (int)skillName), baby.PersonId));
                        }
                    }
                }
                dc.PersonalSkills.AddRange(skillsToAdd);
                dc.SaveChanges();
            }
        }
        private void conceiveAndBirthBabies()
        {
            BirthBabies();
            ConceiveBabies();
        }


        private void estatesMakeProfit()
        {
            using (var dc = new TenderContext())
            {
                foreach (var ownership in dc.EstateOwnerships)
                {
                    var estate = dc.Estates.Single(x => x.Id == ownership.EstateId);
                    var owner = dc.Persons.Single(x => x.PersonId == ownership.OwnerId);

                    double profit = estate.Size * estate.Wealth * PersonRepository.GetSkill(owner.PersonId, (int)SkillName.Moneymaking) / 12;
                    profit += estate.TaxFraction;
                    int collectedTax = (int)profit;
                    estate.TaxFraction = profit - collectedTax;
                    owner.Wealth += collectedTax;
                }
                dc.SaveChanges();
            }
        }

        private void ageEveryLivingPersonOneMonth()
        {
            using (var dc = new TenderContext())
            {
                Date date = dc.getGameState().Date;

                foreach (TenderEntities.Person p in dc.Persons.Where(x => x.Born && !x.Dead))
                {
                    PersonRepository.agePersonOneMonth(p);
                    if (p.IsLiving())
                    {
                        SkillsetRepository.CountSkills(dc, dc.PersonalSkills.Where(x => x.PersonId == p.PersonId).ToList(), p.AgeInMonths);
                    }
                }
                dc.SaveChanges();
            }
        }

        public void addRandomEstate()
        {
            using (var dc = new TenderContext())
            {
                var potentialOwners = dc.Persons.Where(x => x.Born && !x.Dead).Select(x => x.PersonId).ToList();
                if (potentialOwners.Count() > 0)
                {
                    var estate = EntityFactory.RandomEstate();
                    var luckyNumber = random.Next(potentialOwners.Count());
                    var ownerId = potentialOwners.ElementAt(luckyNumber);
                    dc.Estates.Add(estate);
                    dc.SaveChanges();
                    dc.EstateOwnerships.Add(new EstateOwnership() { OwnerId = ownerId, EstateId = estate.Id });
                    dc.SaveChanges();
                }
            }
        }

        //public IList<Person> getLivingPeople()
        //{
        //    using (var dc = new TenderContext())
        //    {
        //        Date date = dc.getGameState().Date;

        //        var list = (from person in dc.Persons.Where(x => x.Born && !x.Dead)
        //                    select person).ToList();
        //        var news = (from items in list
        //                    select new Person()).ToList();
        //        return news;
        //    }
        //}





        ///// <summary>
        ///// Adds an existing person to a player without changing dynasty ownership
        ///// </summary>
        ///// <param name="person">The person whose player is changed</param>
        ///// <param name="player">The new player of the person</param>
        //private void addPersonToPlayer(Person person, Player player)
        //{
        //    person.appointToPlayer(player);
        //    player.addPerson(person);
        //}

        private void advanceWorldDate()
        {
            //date.AdvanceOneMonth();
            using (var dc = new TenderContext())
            {
                TenderEntities.GameState gs = dc.getGameState();
                int newMonth = dc.getGameState().Month + 1;
                gs.Month = newMonth <= 12 ? newMonth : 1;
                gs.Year = newMonth <= 12 ? gs.Year : gs.Year + 1;
                dc.SaveChanges();
            }
        }
    }
}
