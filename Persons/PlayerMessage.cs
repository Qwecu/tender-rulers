﻿using System;

namespace Persons
{
    [Serializable]
    internal class PlayerMessage
    {
        public PlayerMessageType MessageType { get; private set; }
        public string message { get; private set; }
        public PersonRepository RelevantPerson { get; private set; }

        internal PlayerMessage(PlayerMessageType messageType, string message, PersonRepository relevantPerson = null)
        {
            this.MessageType = messageType;
            this.message = message;
            RelevantPerson = relevantPerson;
        }
        public override string ToString()
        {
            return message;
        }
    }
    internal enum PlayerMessageType
    {
        Error,
        PCDiedOfOldAge,
        ReceivedProposal,
        CompetitionWon
    }
}