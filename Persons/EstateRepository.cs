﻿using System;
using TenderRulers;

namespace Persons
{
    [Serializable]
    public class EstateRepository
    {
        public double size { get; private set; }
        public string name { get; private set; }

        private double wealth;


        /// <summary>
        /// used to contain the < 1.0 uncollectable part of taxes between months
        /// </summary>
        private double taxFraction;

        public EstateRepository()
        {
            name = NameObject.randomNameString();
            size = GlobalSettings.randomEstateSize();
            wealth = GlobalSettings.randomEstateWealth();
        }
        public override string ToString()
        {
            return name + " " + (int)size + " km², wealth: " + Math.Round(wealth, 2);
        }
        /// <summary>
        /// Collects taxes from the estate according to the person's moneymaking skill and adds it to their wealth
        /// </summary>
        /// <param name="e">The estate to be taxed</param>
        /// <param name="p">The person who collects the taxes</param>
        //static internal void profit(Estate e, Person p)
        //{
        //    double profit = e.size * e.wealth * p.getSkill(SkillName.Moneymaking) / 12;
        //    profit += e.taxFraction;
        //    int collectedTax = (int)profit;
        //    e.taxFraction = profit - collectedTax;
        //    p.increaseWealth(collectedTax);
        //}
    }
}