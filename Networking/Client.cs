﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Timers;

namespace Networking
{
    public class Client
    {
        private ClientServerConnection serverConnection;
        private IPAddress ipa;
        private int port;
        private IPEndPoint endpoint;
        private Socket socket;
        private System.Threading.Timer timer;
        private object timerLock;
        public int updateInterval { get; set; }
        private bool canExposeCheckServerInput;
        public bool connected;

        /// <summary>
        /// Raised when the client has received a byte[] from the server
        /// </summary>
        public event Action<object, DataReceivedEventArgs> DataReceived;

        /// <summary>
        /// Instantiates a client that raises DataReceived in a different thread using System.Timers.Timer
        /// </summary>
        /// <param name="ipaddress">The IP Address to connect to</param>
        /// <param name="port">The port to connect to</param>
        public Client(string ipaddress, int port)
        {
            ipa = IPAddress.Parse(ipaddress);
            this.port = port;
            endpoint = new IPEndPoint(ipa, port);
            updateInterval = 100;
            timerLock = new object();
            canExposeCheckServerInput = false;
            connected = false;
        }

        /// <summary>
        /// Instantiates a client using Unity's Update method
        /// </summary>
        /// <param name="ipaddress"></param>
        /// <param name="port"></param>
        /// <param name="UnityUpdate"></param>
        public Client(string ipaddress, int port, bool fromUnity)
        {
            ipa = IPAddress.Parse(ipaddress);
            this.port = port;
            endpoint = new IPEndPoint(ipa, port);
            updateInterval = 100;
            canExposeCheckServerInput = true;
            connected = false;
        }

        public void updateFromUnity()
        {
            if (canExposeCheckServerInput)
            {
                checkServerInput();
            }
            else
            {
                throw new InvalidOperationException("Not allowed with current constructor");
            }
        }


        /// <summary>
        /// The DataReceived event is raised through this method
        /// </summary>
        /// <param name="message">an EventArgs object containing the received data</param>
        private void OnDataReceived(DataReceivedEventArgs message)
        {
            var handler = DataReceived;
            if (handler != null)
            {
                handler(this, message);
            }
        }

        /// <summary>
        /// Connects the client to the specified server and starts listening to incoming messages
        /// </summary>
        public void Connect()
        {
            try
            {
                initializeSocket();
                serverConnection = new ClientServerConnection(socket);
                /* timer = new System.Timers.Timer(updateInterval);
                 timer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);
                 timer.Enabled = true;*/
                timer = new System.Threading.Timer(checkServerInput, null, 0, updateInterval);
                connected = true;
            }
            catch (SocketException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Server input is checked through this method in an asynchronous environment
        /// </summary>
        /// <param name="state"></param>
        private void checkServerInput(object state)
        {
            lock (timerLock)
            {
                checkServerInput();
            }
        }

        /// <summary>
        /// Can be called directly in a synchronous environment
        /// </summary>
        private void checkServerInput()
        {
            if (serverConnection == null)
            {
                throw new InvalidOperationException("The client is not connected");
            }
            byte[] message = serverConnection.receiveMessage();

            if (message != null)
            {
                Console.WriteLine("Message received: " + message.Length);
                OnDataReceived(new DataReceivedEventArgs(message));
            }
        }

        /// <summary>
        /// Sends a byte array through the connection or throws an exception if failed
        /// </summary>
        /// <param name="data"></param>
        public void send(byte[] data)
        {
            if (serverConnection == null)
            {
                throw new InvalidOperationException("The client is not connected");
            }
            serverConnection.sendData(data);
        }

        private void initializeSocket()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endpoint);
        }
    }

    /// <summary>
    /// An EventArgs class containing a byte array (message) and a ClientID
    /// </summary>
    public class DataReceivedEventArgs : EventArgs
    {
        public byte[] data { get; private set; }
        public ClientID clientID { get; private set; }
        public DataReceivedEventArgs(byte[] message)
        {
            data = message;
        }
        public DataReceivedEventArgs(byte[] message, ClientID id)
        {
            data = message;
            clientID = id;
        }
    }
}
