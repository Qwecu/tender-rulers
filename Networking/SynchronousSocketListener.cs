﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

namespace Networking
{
    public class SynchronousSocketListener
    {
        private int dataBufferSize;
        private ushort port;
        private IPEndPoint ipEndPoint;
        private Dictionary<ClientID, ClientServerConnection> clientConnections;
        private Socket listenerSocket;
        private System.Threading.Timer timer;
        private object timerLock;

        private int refreshInterval;
        public event Action<object, DataReceivedEventArgs> DataReceived;
        public event Action<object, ClientConnectionsRefreshedEventArgs> ClientConnectionsRefreshed;

        public void SendToSpecific(byte[] data, ClientID id)
        {
            ClientServerConnection connection = clientConnections[id];
            if (connection != null)
            {
                trySend(connection, data);
            }            
        }

        /// <summary>
        /// The constructor. Initialize() has to be called next.
        /// </summary>
        /// <param name="port">The port number</param>
        /// <param name="refreshInterval"></param>
        public SynchronousSocketListener(ushort port, int refreshInterval)
        {
            clientConnections = new Dictionary<ClientID, ClientServerConnection>();

            dataBufferSize = 1024;
            ClientServerConnection.dataBufferSize = dataBufferSize;

            this.port = port;

            this.refreshInterval = refreshInterval;
            timerLock = new object();
        }

        public void Initialize()
        {
            listenerSocket = CreateListenerSocket();
            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                InitializeListenerSocket();

                timer = new System.Threading.Timer(Refresh, null, 0, refreshInterval);
                Console.WriteLine("Waiting for a connection in port " + port.ToString() + "...");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }  

        private void Refresh(object state)
        {
            lock (timerLock)
            {
                RefreshConnections();
                GetNewClientMessages();
            }            
        }

        public void SendToAll(byte[] data)
        {
            Console.WriteLine("Sending " + data.Length + " bytes to " + clientConnections.Count);
            foreach (ClientServerConnection connection in clientConnections.Values)
            {
                trySend(connection, data);                
            }
        }

        private void trySend(ClientServerConnection connection, byte[] data)
        {
            try
            {
                connection.sendData(data);
            }
            catch (SocketException e)
            {
                if (e.ErrorCode != 10054)

                {
                    Console.WriteLine(e.Message + " error code " + e.ErrorCode);
                    throw;
                }
            }
        }

        private void RefreshConnections()
        {
            bool somethingHasChanged = false;
            if (listenerSocket == null)
            {
                throw new InvalidOperationException("A listener socket has not been initialised");
            }
            Socket handler = AcceptNewConnection(listenerSocket);

            if (handler != null)
            {
                handler.Blocking = false;
                clientConnections.Add(new ClientID(), new ClientServerConnection(handler));
                Console.WriteLine("New connection");
                somethingHasChanged = true;
            }
            var expiredConnections = new List<ClientID>();
            foreach(ClientID id in clientConnections.Keys)
            {
                ClientServerConnection cc = clientConnections[id];
                if (cc.Expired == true)
                {
                    expiredConnections.Add(id);
                    Console.WriteLine(cc + " left the game");
                    somethingHasChanged = true;
                }
            }
            foreach (ClientID id in expiredConnections)
            {
                clientConnections.Remove(id);
            }
            if (somethingHasChanged)
            {
                OnClientConnectionsRefreshed();
            }
        }

        private void OnClientConnectionsRefreshed()
        {
            var handler = ClientConnectionsRefreshed;
            if (handler != null)
            {
                List<ClientID> IDS = new List<ClientID>();
                foreach (ClientID id in clientConnections.Keys)
                {
                    IDS.Add(id);
                }
                var eventArgs = new ClientConnectionsRefreshedEventArgs(IDS);
                handler(this, eventArgs);
            }
        }

        private void GetNewClientMessages()
        {
            foreach (ClientID id in clientConnections.Keys)
            {
                try
                {
                    ClientServerConnection connection = clientConnections[id];
                    byte[] message = connection.receiveMessage();
                    if (message != null)
                    {
                        OnDataReceived(message, id);
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private void OnDataReceived(byte[] message, ClientID id)
        {
            var handler = DataReceived;
            if (handler != null)
            {
                handler(this, new DataReceivedEventArgs(message, id));
            }
        }

        private void InitializeListenerSocket()
        {
            listenerSocket.Bind(ipEndPoint);
            listenerSocket.Listen(10);
            listenerSocket.Blocking = false;
        }

        private Socket AcceptNewConnection(Socket listener)
        {
            try
            {
                return listener.Accept();
            }
            catch (SocketException e)
            {
                if (e.ErrorCode == 10035)
                {
                    return null;
                }
                else
                {
                    Console.WriteLine("When accepting " + e.ErrorCode);
                    throw;
                }
            }
        }

        private static void CloseHandler(Socket handler)
        {
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
            Console.WriteLine("Closed connection " + handler);
        }


        private Socket CreateListenerSocket()
        {
            IPAddress ipAddr = IPAddress.Loopback;
            ipEndPoint = new IPEndPoint(ipAddr, port);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream, ProtocolType.Tcp);
            return listener;
        }
    }

    public class ClientConnectionsRefreshedEventArgs : EventArgs
    {
        public List<ClientID> currentConnections { get; private set; }
        public ClientConnectionsRefreshedEventArgs(List<ClientID> connections)
        {
            currentConnections = connections;
        }
    }
}
