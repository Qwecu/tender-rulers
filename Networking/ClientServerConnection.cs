﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Networking
{
    public class ClientServerConnection
    {
        public Socket handler { get; set; }
        public bool Expired { get; private set; }

        public static int dataBufferSize;
        //private byte[] dataBuffer;
        private List<byte> buffer;
        private int? messageLength;

        static ClientServerConnection()
        {
            dataBufferSize = 1024;
        }

        public ClientServerConnection(Socket handler)
        {
            this.handler = handler;
            Expired = false;
        }

        /// <summary>
        /// Receives a byte array through the connection.
        /// </summary>
        /// <returns>The received byte array</returns>
        public byte[] receiveMessage()
        {
            if (buffer == null)
            {
                buffer = new List<byte>();
            }
            var dataBuffer = new byte[dataBufferSize];
            int bytesRec = 0;
            try
            {
                //Console.WriteLine("Receiving: " + bytesRec);
                bytesRec = handler.Receive(dataBuffer);
                //Console.WriteLine("Bytes received: " + bytesRec);
            }
            catch (SocketException e)
            {
                if (e.ErrorCode != 10035)
                {
                    Expired = true;
                }
            }

            if (bytesRec > 0)
            {
                for (int i = 0; i < bytesRec; i++)
                {
                    buffer.Add(dataBuffer[i]);                    
                }
                if (buffer.Count >= 4 && messageLength == null)
                {
                    messageLength = BitConverter.ToInt32(buffer.ToArray(), 0);
                }
                if (messageLength != null && buffer.Count >= messageLength + 4)
                {
                    buffer.RemoveRange(0, 4);
                    byte[] message = buffer.ToArray();
                    messageLength = null;
                    buffer = null;
                    return message;
                }
            }
            return null;
        }

        /// <summary>
        /// Sends a byte array through the connection.
        /// </summary>
        /// <param name="dataToSend">The byte array to be sent</param>
        public void sendData(byte[] dataToSend)
        {
            try
            {
                byte[] lengthInfo = BitConverter.GetBytes(dataToSend.Length);

                handler.Send(lengthInfo);
                handler.Send(dataToSend);
                Console.WriteLine("Sending succeeded");
            }
            catch (SocketException e)
            {
                Console.WriteLine("Sending failed");
                if (e.ErrorCode != 10035)
                {
                    Expired = true;
                    Console.WriteLine(e.Message);
                    //throw;
                }
            }
        }
    }
}
