﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Networking
{
    class Program
    {
        SynchronousSocketListener server;
        List<Client> clients;

        static void client_MessageReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("Data yippee!");
            Console.Read();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.server = new SynchronousSocketListener(9369, 1000);
            p.server.Initialize();

            p.clients = new List<Client>();
            Client client = new Client("127.0.0.1", 9369);
            client.updateInterval = 1000;
            client.DataReceived += client_MessageReceived;
            client.Connect();
            p.clients.Add(client);

            Thread.Sleep(1000);
            p.server.SendToAll(new byte[1600]);

            Console.ReadKey();
        }
    }
}
