﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ClientApplication;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();
            App.client.GameStateRefreshed += GameState_Refreshed;
            App.client.StringReceived += Client_StringReceived;
        }

        private void Client_StringReceived(object sender, StringReceivedEventArgs e)
        {
            MessageBox.Show(e.message);
        }

        private void GameState_Refreshed(object sender, GameStateRefreshedEventArgs e)
        {
            App.gameState = e.gameState;
            var messages = App.gameState.getMessages();
            Console.WriteLine(string.Format("{0} messages", messages.Count()));
           /* foreach (string s in App.gameState.getMessages())
            {
                Console.WriteLine(s);
            }*/
            //list.ItemsSource = App.gameState.getMessages();
            list.Dispatcher.Invoke(() => list.ItemsSource = App.gameState.getMessages());
        }

        private void connect_Click(object sender, RoutedEventArgs e)
        {
            string name = playerName.Text;
            string password = this.password.Password;
            if (password.Equals(""))
            {
                MessageBox.Show("Your password is too short, please select a longer one", "Invalid password");
                return;
            }
            Console.WriteLine("Connecting " + name);
            App.connectPlayer(name, password);
            if (App.gameState == null)
            {
                return;
            }
            var messages = App.gameState.getMessages();
            Console.WriteLine(string.Format("{0} messages", messages.Count()));
            foreach(string s in App.gameState.getMessages())
            {
                Console.WriteLine(s);
            }
            list.ItemsSource = App.gameState.getMessages();
        }

        private void MyPersons_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MyPersonsView());
        }
    }
}
