﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Persons;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for DynastyView.xaml
    /// </summary>
    public partial class DynastyView : Page
    {
        private DynastyRepository dynasty;

        public DynastyView()
        {
            InitializeComponent();
        }

        public DynastyView(DynastyRepository dynasty)
        {
            InitializeComponent();
            this.dynasty = dynasty;
            peopleBox.ItemsSource = dynasty.livingPeople();
            foreach(DynastyCouncilChair chair in Enum.GetValues(typeof(DynastyCouncilChair)))
            {
                var tb = new Button();
                tb.Margin = new Thickness(5);
                StringBuilder sb = new StringBuilder();
                sb.Append(chair.ToString() + "\n");
                PersonRepository chairHolder = dynasty.getCouncilMember(chair);
                sb.Append(chairHolder == null ? "none" : chairHolder.Name);
                sb.Append("\n");
                tb.Content = sb.ToString();
                tb.Click += Tb_Click;
                DockPanel.Children.Add(tb);
            }
        }

        private void Tb_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(sender.GetType().ToString());
        }
    }
}
