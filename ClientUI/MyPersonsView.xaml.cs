﻿using Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for MyPersonsView.xaml
    /// </summary>
    public partial class MyPersonsView : Page
    {
        public MyPersonsView()
        {
            InitializeComponent();
            App.client.GameStateRefreshed += Client_GameStateRefreshed;
            if (App.gameState != null)
            {
                peopleBox.ItemsSource = App.gameState.getLivingPeople((App.gameState.getPlayer(App.playerName)));
            }            
        }

        private void Client_GameStateRefreshed(object arg1, ClientApplication.GameStateRefreshedEventArgs arg2)
        {
            peopleBox.Dispatcher.Invoke(() => peopleBox.ItemsSource = App.gameState.getLivingPeople((App.gameState.getPlayer(App.playerName))));
            
        }

        private void newPerson_Click(object sender, RoutedEventArgs e)
        {
            App.AddNewPlayerCharacter();
        }

        private void dynastyView_Click(object sender, RoutedEventArgs e)
        {
            object obj = peopleBox.SelectedItem;
            var person = obj as PersonRepository;
            if (person != null)
            {
                NavigationService.Navigate(new DynastyView(person.dynasty));
            }
        }
    }
}
