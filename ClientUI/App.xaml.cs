﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ClientApplication;
using Persons;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static ClientApplication.Program client = new Program();
        internal static GameStateRepository gameState;
        internal static string playerName;

        internal static void connectPlayer(string name, string password)
        {
            client.connectPlayer(name, password);
            playerName = name;
           // gameState = client.getGameState();
        }

        internal static void AddNewPlayerCharacter()
        {
            client.AddNewPlayerCharacter();
        }
    }
}
