﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderRulers
{
    [Serializable]
    internal class Syllable
    {
        internal readonly string beginningConsonant;
        internal readonly string vowel;
        internal readonly string endingConsonant;

        internal Syllable(string cons1, string vowel, string cons2)
        {
            beginningConsonant = cons1;
            this.vowel = vowel;
            endingConsonant = cons2;
        }

        /// <summary>
        /// Makes a syllable combining parts of two existing syllables randomly
        /// </summary>
        /// <param name="mother">Input syllable 1</param>
        /// <param name="father">Input syllable 2</param>
        public Syllable(Syllable mother, Syllable father)
        {
            //The syllable is either identical to one of the parents...
            int rand = NameObject.random.Next(10);
            if(rand < 2)
            {
                beginningConsonant = rand == 0 ? mother.beginningConsonant : father.beginningConsonant;
                vowel = rand == 0 ? mother.vowel : father.vowel;
                endingConsonant = rand == 0 ? mother.endingConsonant : father.endingConsonant;
            }
            //...or combined of ther parts
            else
            {
                rand = NameObject.random.Next(2);
                beginningConsonant = rand == 0 ? mother.beginningConsonant : father.beginningConsonant;
                rand = NameObject.random.Next(2);
                vowel = rand == 0 ? mother.vowel : father.vowel;
                rand = NameObject.random.Next(2);
                endingConsonant = rand == 0 ? mother.endingConsonant : father.endingConsonant;
            }            
        }

        public override string ToString()
        {
            return (beginningConsonant + vowel + endingConsonant);
        }
    }
}
