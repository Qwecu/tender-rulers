﻿using System;
using System.Text;

namespace TenderRulers
{
    [Serializable]
    public class NameObject
    {
        /// <summary>
        /// Creates a random name with three syllables.
        /// </summary>
        private string name;
        internal static Random random;
        private static SyllableObjectBuilder sob;
        private Syllable[,] nameGenes;

        private static int chromosomes;
        private static int locuses;

        static NameObject()
        {
            random = GlobalSettings.random;
            sob = new SyllableObjectBuilder();
            //two chromosomes, one holding the dominant genes and the other the recessive genes
            chromosomes = 2;
            //every locus holds one syllable of the name
            locuses = 3;
        }

        public static string[] NameArray(int syllables)
        {
            var sylarray = new string[syllables * 3];
            for(int i = 0; i < syllables; i++)
            {
                var syl = randomSyllable();
                sylarray[i * 3] = syl.beginningConsonant;
                sylarray[i * 3 + 1] = syl.vowel;
                sylarray[i * 3 + 2] = syl.endingConsonant;
            }
            return sylarray;
        }

        public NameObject()
        {
            nameGenes = new Syllable[chromosomes, locuses];
            for (int i = 0; i < chromosomes; i++)
            {
                for (int j = 0; j < locuses; j++)
                {

                    nameGenes[i, j] = randomSyllable();
                }
            }
            constructDisplayedName();
        }

        public NameObject(NameObject mother, NameObject father = null)
        {
            nameGenes = new Syllable[chromosomes, locuses];
            if (father == null)
            {
                father = new NameObject();
            }
            for (int i = 0; i < locuses; i++)
            {
                Syllable motherMix = new Syllable(mother.nameGenes[0, i], mother.nameGenes[1, i]);
                Syllable fatherMix = new Syllable(father.nameGenes[0, i], father.nameGenes[1, i]);
                int dominant = random.Next(2);
                nameGenes[0, i] = dominant == 0 ? motherMix : fatherMix;
                nameGenes[1, i] = dominant == 0 ? fatherMix : motherMix;
            }
            constructDisplayedName();
        }

        /// <summary>
        /// actual displayed name is consturucted of the dominant genes
        /// </summary>
        private void constructDisplayedName()
        {
            var sb = new StringBuilder(15);
            for (int i = 0; i < nameGenes.GetLength(1); i++)
            {
                sb.Append(nameGenes[0, i]);
            }
            name = sb.ToString().Substring(0, 1).ToUpper() + sb.ToString().Substring(1);
        }

        //returns a random name in plain string format
        public static string randomNameString()
        {
            string a = ("" + randomSyllable() + randomSyllable() + randomSyllable());
            a = a.Substring(0, 1).ToUpper() + a.Substring(1);
            return a;
        }

        private static Syllable randomSyllable()
        {
            return SyllableObjectBuilder.randomSyllable();
        }

        public override string ToString()
        {
            return name;
        }

        public string[] RandomSyllable()
        {
            var syllable = SyllableObjectBuilder.randomSyllable();
            return new string[] { syllable.beginningConsonant, syllable.vowel, syllable.endingConsonant };
        }
    }

    internal class SyllableObjectBuilder
    {
        private static string[] beginningCharacterList = { "", "b", "c", "ch", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z" };
        private static int[] beginningWeights = { 90, 10, 10, 10, 10, 10, 10, 10, 10, 10, 30, 30, 30, 30, 3, 20, 20, 20, 10, 5, 5, 5, };
        private static string[] vowelList = { "a", "ai", "e", "i", "o", "ou", "u", "y" };
        private static int[] vowelWeights = { 15, 2, 12, 12, 10, 2, 8, 5 };
        private static string[] endingCharacterList = { "", "b", "c", "ch", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z" };
        private static int[] endingWeights = { 300, 10, 10, 7, 10, 8, 8, 8, 2, 13, 13, 13, 13, 13, 3, 13, 13, 13, 10, 3, 3, 3, };

        public static Syllable randomSyllable()
        {
            return new Syllable(
                pickCharacter(beginningCharacterList, beginningWeights),
                pickCharacter(vowelList, vowelWeights),
                pickCharacter(endingCharacterList, endingWeights));
        }

        private static string pickCharacter(string[] list, int[] weights) //picks a random String from the list weighed with the values from weights
        {
            if (list.Length == 0 || weights.Length == 0 || list.Length != weights.Length)
            {
                throw new Exception("The lengths of the lists must be equal and greater than zero");
            }
            int sumOfWeights = 0;
            foreach (int i in weights)
            {
                if (i >= 0) //negative weights are considered zero
                    sumOfWeights += i;
            }
            if (sumOfWeights == 0)
            { //if all weights are 0, a random string is returned
                return list[NameObject.random.Next(list.Length)];
            }

            int n = NameObject.random.Next(sumOfWeights);

            for (int i = 0; i < list.Length; i++)
            {
                if (weights[i] > n)
                {
                    return list[i];
                }
                n -= weights[i];
            }
            throw new Exception("Non-matching weights and sum of weights"); //this should never happen
        }
    }
}