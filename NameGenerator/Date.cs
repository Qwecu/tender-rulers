﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderRulers
{
    [Serializable]
    public struct Date
    {
        public int Year { get; private set; }
        public int Month { get; private set; }

        public Date(int year, int month)
        {
            if (month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be between 1 and 12");
            }
            this.Year = year;
            this.Month = month;
        }
        public int InMonths()
        {
            return Year * 12 + Month;
        }
        internal void AdvanceOneMonth()
        {
            Month++;
            if (Month == 13)
            {
                Month = 1;
                Year++;
            }
        }
        public static bool operator <(Date d1, Date d2)
        {
            if (d1.Year < d2.Year) return true;
            if (d1.Year > d2.Year) return false;
            if (d1.Month < d2.Month) return true;
            return false;
        }
        public static bool operator >(Date d1, Date d2)
        {
            if (d1.Year > d2.Year) return true;
            if (d1.Year < d2.Year) return false;
            if (d1.Month > d2.Month) return true;
            return false;
        }
        public static bool operator <=(Date d1, Date d2)
        {
            if (d1.Year < d2.Year) return true;
            if (d1.Year > d2.Year) return false;
            if (d1.Month <= d2.Month) return true;
            return false;
        }
        public static bool operator >=(Date d1, Date d2)
        {
            if (d1.Year > d2.Year) return true;
            if (d1.Year < d2.Year) return false;
            if (d1.Month >= d2.Month) return true;
            return false;
        }
        public static bool operator ==(Date d1, Date d2)
        {
            if (d1.Year == d2.Year && d1.Month == d2.Month)
            {
                return true;
            }
            return false;
        }
        public static bool operator !=(Date d1, Date d2)
        {
            if (d1.Year != d2.Year || d1.Month != d2.Month)
            {
                return true;
            }
            return false;
        }
        public static Date operator -(Date d1, Date d2)
        {
            int y = d1.Year - d2.Year;
            int m = d1.Month - d2.Month;
            if (m < 1)
            {
                m += 12;
                y--;
            }
            return new Date(y, m);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is Date)) return false;
            return (this == (Date)obj);
        }
        public override int GetHashCode()
        {
            return Year * 3 + Month * 7;
        }
    }
}
