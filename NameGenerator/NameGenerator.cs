﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TenderRulers
{
    class Program
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)")]
        static void Main(string[] args)
        {
            Console.WriteLine("Hello. This is a random name generator. Today's first five names are");
            for (int i = 0; i < 5; i++)
                Console.WriteLine(new NameObject());
            Console.WriteLine("Press M for a new name. Press any other key to exit.");
            while (Console.ReadKey().Key == ConsoleKey.M)
            {
                Console.WriteLine("\n" + new NameObject());
            }
        }
    }
}
