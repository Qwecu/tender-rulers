﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderRulers
{
    public static class GlobalSettings
    {
        public static Random random;
        public static int MaxPopulation => 200;
        public static int minDynasties => 60;
        public static double workNeededPerCompetition => 100.0;
        public static int messageBufferSize => 100;
        public static int maxPCsPerPlayer => 5;
        public static int MinMarryingAge => 15;//15;
        public static int PlayerMessageListLength => 100;
        public static int DefaultDyingAge => 50;
        public static int DefaultDyingAgeMeanVariance => 15;
        public static double ShortNameLimit => 1.0;
        public static double LongNameLimit => 3.0; //these values between 0.0 and 4.0
        public static int RandomAdjectiveId => random.Next(1896) + 1;
        public static int RandomNounId => random.Next(980) + 1;
        public static int RandomAdverbId => random.Next(331) + 1;
        public static int RandomVerbId => random.Next(2503) + 1;



        static GlobalSettings()
        {
            random = new Random();
        }
        public static int RandomDyingAgeInMonths()
        {
            return (int)NextGaussian(DefaultDyingAge * 12.0, DefaultDyingAgeMeanVariance * 12.0);
        }
        public static double DefaultMaxSkill => 20.0;

        /// <summary>
        /// this sets the minimum limits of dynasty glory to get the council chairs available
        /// </summary>
        public static double[] gloryLimits = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        public static double randomEstateSize() => random.NextDouble() + random.NextDouble() * 100;
        public static double randomEstateWealth() => random.NextDouble() + random.NextDouble();


        public static bool willIConceive(double fertility, int populationCount)
        {
            if (populationCount > MaxPopulation) return false;
            //double fertility = procreator.getFertility();
            double i = random.NextDouble();
            if (i < fertility) return true;
            return false;
        }

        public static double MonthlyGloryIncreaseFromOnePersonToTheirDynasty => 0.005;        

        public static Tuple<int, int> randomMaxAgeYearsMonths()
        {
            int monthsToLive = (int)NextGaussian(600.0, 200.0);
            int yearsToLive = monthsToLive / 12;
            return Tuple.Create(yearsToLive, monthsToLive % 12);
        }

        public static double NextGaussian(double mu = 0, double sigma = 1)
        {
            var u1 = random.NextDouble();
            var u2 = random.NextDouble();

            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2);

            var randNormal = mu + sigma * randStdNormal;

            return randNormal;
        }

        public static int yearlyTaxFromDynastyMemberToTheirDynasty(double stewardSkill, int personalWealth)
        {
            //Person steward = dynasty.getCouncilMember(DynastyCouncilChair.Steward);
            //if (steward == null) return 0;
            //double stewardSkill = steward.getSkill(SkillName.Moneymaking);
            int tax = (int)(stewardSkill / 100 * personalWealth);
            return tax;
        }

        public static double DecreaseDynastyGloryMonthly => 0.99;

        public static double DecreasePersonGloryMonthly => 0.99;

        public static int RandomAgeInMonthsNewPerson => (int)NextGaussian(240, 36);
    }
    //Enum members can be added freely between builds
    public enum SkillName
    {
        Moneymaking, Charisma, Organizing, Strength, Agility, Creativity, Fertility
    }

    public enum Gender
    {
        female, male
    }

    public enum DynastyCouncilChair
    {
        Dictator, Warrior, Steward, Diplomat, MC, Architect
    }

    public enum ParentChildRelationshipType
    {
        UnbornInWomb, Mother, Father, GeneticFather
    }

    public enum GeneType
    {
        LuckFactorInCompetitions
    }
}
