﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MyGame.aspx.cs" Inherits="MyGame" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br />
    <asp:Label runat="server">Player name</asp:Label>
    <br />
    <asp:TextBox ID ="txtPlayerName" runat="server" ></asp:TextBox>
    <br />
    <asp:Button ID="btnSavePlayerName" runat="server" Text="Save name" OnClick="btnSavePlayerName_Click" CssClass="btn btn-primary" />
     <br />
     <asp:Label runat="server">My persons</asp:Label>
    <br />
    <div class="table-responsive" >
    <table class="table table-striped">
        <thead>
            <tr>
            <th><asp:Label runat="server" Text="Name"></asp:Label></th>
            <th><asp:Label runat="server" Text="Dynasty"></asp:Label></th>
            <th><asp:Label runat="server" Text="Age"></asp:Label></th>
            <th><asp:Label runat="server" Text="Glory"></asp:Label></th>
            <th><asp:Label runat="server" Text="Wealth"></asp:Label></th>
            <th><asp:Label runat="server" Text="Castle"></asp:Label></th>
                </tr>
        </thead>
    <asp:ListView ID="lvPersons" runat="server" ItemType="Persons.PersonRepository.PersonListItem" OnItemDataBound="lvPersons_ItemDataBound">
        <ItemTemplate>
            <tr>
            <td><asp:Label ID="lblName" runat="server" Text='<%#Item.Name%>'></asp:Label></td>
            <td><asp:Label ID="lblDynasty" runat="server" Text='<%#Item.DynastyName%>'></asp:Label></td>
            <td> <asp:Label ID="lblAge" runat="server" Text='<%#Item.Age%>'></asp:Label></td>
            <td><asp:Label ID="lblGlory" runat="server" Text='<%#Item.Glory%>'></asp:Label></td>
            <td> <asp:Label ID="lblWealth" runat="server" Text='<%#Item.Wealth%>'></asp:Label></td>
            <td><asp:Label ID="lblCastle" runat="server" Text='<%#Item.Castle%>'></asp:Label></td>
             </tr>
        </ItemTemplate>
    </asp:ListView>
    </table>
        </div>
     <br />
    <asp:Button ID="btnNewPerson" CssClass="btn btn-primary" Text="Create a new person"  runat="server" OnClick="btnNewPerson_Click"/>
     <br />
</asp:Content>

