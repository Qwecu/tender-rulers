﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Persons;
using TenderEntities;
using TenderRulers;

public partial class Competitions : System.Web.UI.Page
{
    int _playerId;
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
        var id = Context.User.Identity.GetUserId();
        var player = Persons.PlayerRepository.GetPlayer(id);
        _playerId = player.Id;
        if (!Page.IsPostBack)
        {
            FillForm(player);
        }
    }

    private void FillForm(TenderEntities.Player player)
    {
        List<TenderEntities.Person> persons = Persons.PersonRepository.GetLivingPersonsOfPlayer(_playerId);
        txtPlayerName.Text = player.Name;
        lvPersons.DataSource = persons;
        lvPersons.DataBind();

        if (persons.Count() >= GlobalSettings.maxPCsPerPlayer)
        {
            btnNewPerson.Visible = false;
        }
    }

    protected void lvPersons_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

    }

    protected void btnNewPerson_Click(object sender, EventArgs e)
    {
        PersonRepository.AddNewRandomPerson(_playerId);
        FillForm(PlayerRepository.GetPlayer(_playerId));
    }

    protected void btnSavePlayerName_Click(object sender, EventArgs e)
    {
        PlayerRepository.UpdatePlayerName(_playerId, txtPlayerName.Text);
    }
}