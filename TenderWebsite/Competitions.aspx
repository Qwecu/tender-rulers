﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Competitions.aspx.cs" Inherits="Competitions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br />
    <asp:Label runat="server">Player name</asp:Label>
    <br />
    <asp:TextBox ID ="txtPlayerName" runat="server"></asp:TextBox>
    <br />
    <asp:Button ID="btnSavePlayerName" runat="server" Text="Save name" OnClick="btnSavePlayerName_Click" />
     <br />
     <asp:Label runat="server">My persons</asp:Label>
    <br />
    <asp:ListView ID="lvPersons" runat="server" ItemType="TenderEntities.Person" OnItemDataBound="lvPersons_ItemDataBound">
        <ItemTemplate>
            <asp:Label ID="lblName" runat="server" Text='<%#Item.Name%>'></asp:Label>
            <asp:Label ID="lblDynasty" runat="server" Text='<%#Item.DynastyId%>'></asp:Label>
            <asp:Label ID="lblAge" runat="server" Text='<%#Item.AgeInMonths / 12%>'></asp:Label>
            <asp:Label ID="lblGlory" runat="server" Text='<%#(int)Item.Glory%>'></asp:Label>
            <asp:Label ID="lblWealth" runat="server" Text='<%#Item.Wealth%>'></asp:Label>
            <asp:Label ID="lblCastle" runat="server" Text=""></asp:Label>
        </ItemTemplate>
    </asp:ListView>
     <br />
    <asp:Button ID="btnNewPerson" CssClass="btn btn-primary" Text="Create a new person"  runat="server" OnClick="btnNewPerson_Click"/>
     <br />
</asp:Content>

