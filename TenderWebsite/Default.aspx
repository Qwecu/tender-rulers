﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Tender Rulers</h1>
        <p class="lead">Welcome to the best game ever made!</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                You can collect money and glory and compete against other people!
            </p>
            <p>
                <a class="btn btn-default" >Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>The Fun</h2>
            <p>
                Play for fun! Lots!
            </p>
            <p>
                <a class="btn btn-default" >Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Other info</h2>
            <p>
                I have nothing more to say!
            </p>
            <p>
                <a class="btn btn-default" >Learn more &raquo;</a>
            </p>
        </div>
    </div>
</asp:Content>
