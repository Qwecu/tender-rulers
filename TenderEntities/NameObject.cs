﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class NameObject
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string DomCons1_1 { get; set; }
        public string RecCons1_1 { get; set; }
        public string DomVow1_2 { get; set; }
        public string RecVow1_2 { get; set; }
        public string DomCons1_3 { get; set; }
        public string RecCons1_3 { get; set; }

        public string DomCons2_1 { get; set; }
        public string RecCons2_1 { get; set; }
        public string DomVow2_2 { get; set; }
        public string RecVow2_2 { get; set; }
        public string DomCons2_3 { get; set; }
        public string RecCons2_3 { get; set; }

        public string DomCons3_1 { get; set; }
        public string RecCons3_1 { get; set; }
        public string DomVow3_2 { get; set; }
        public string RecVow3_2 { get; set; }
        public string DomCons3_3 { get; set; }
        public string RecCons3_3 { get; set; }

        public string DomCons4_1 { get; set; }
        public string RecCons4_1 { get; set; }
        public string DomVow4_2 { get; set; }
        public string RecVow4_2 { get; set; }
        public string DomCons4_3 { get; set; }
        public string RecCons4_3 { get; set; }

        public int SyllableCount { get; set; }

        public double longNameProbability1 { get; set; }
        public double longNameProbability2 { get; set; }
        public double longNameProbability3 { get; set; }
        public double longNameProbability4 { get; set; }
        public int PersonId { get; set; }
    }
}
