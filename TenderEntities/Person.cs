﻿using System;
using TenderRulers;

namespace TenderEntities
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        //public int? NameObjectId { get; set; }
        //public int? BirthYear { get; set; }
       // public int? BirthMonth { get; set; }
        public int? BirthTime { get; set; }
        public int? DeathTime { get; set; }
        public int AgeInMonths { get; set; }
        public bool Born { get; set; }
        public bool Dead { get; set; }
       // public int? DeathYear { get; set; }
        //public int? DeathMonth { get; set; }
        public int? DynastyId { get; set; }
        public int GenderId { get; set; }
        public int Wealth { get; set; }
        public double Glory { get; set; }
        public int? PlayerId { get; set; }

        public int WordVerbId { get; set; }
        public int WordAdverbId { get; set; }
        public int WordNounId { get; set; }
        public int WordAdjectiveId { get; set; }

        public int? PlaceOfResidenceId { get; set; }
        public double FertilityModifier { get; set; }
        public virtual Player FK_Player { get; set; }
        public virtual Dynasty FK_Dynasty { get; set; }
        public virtual WordAdjective WordAdjective { get; set; }
        public virtual WordAdverb WordAdverb { get; set; }
        public virtual WordNoun WordNoun { get; set; }
        public virtual WordVerb WordVerb { get; set; }

        public bool IsLiving()
        {
            return (Born && !Dead);
        }
       
        public string FullName => Name + " " + FK_Dynasty?.Name;

        public Date? Age (Date atDate)
        {
            if (DeathTime.HasValue) return (new Date( (DeathTime.Value - BirthTime.Value) / 12, (DeathTime.Value - BirthTime.Value) % 12));
            if (BirthTime.HasValue && BirthTime > atDate.InMonths()) return (new Date((atDate.InMonths() - BirthTime.Value)/12, (atDate.InMonths() - BirthTime.Value) % 12));
            return null;
        }
    }
}