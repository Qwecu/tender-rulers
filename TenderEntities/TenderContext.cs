﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class TenderContext : DbContext

    {
        public TenderContext()
            : base("TenderRulers") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
       {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //  Configure default schema
            modelBuilder.HasDefaultSchema("dbo");

           // Database.SetInitializer<TenderContext>(null);
        }
        public DbSet<Dynasty> Dynasties { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PlayerMessage> PlayerMessages { get; set; }
        public DbSet<Estate> Estates { get; set; }
        public DbSet<GameState> TheGameState { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<PersonalSkill> PersonalSkills { get; set; }
        public DbSet<Competition> Competitions { get; set; }
        public DbSet<CompetitionSkillRequirement> CompetitionSkillRequirements { get; set; }
        public DbSet<CompetitionSignUp> CompetitionSignUps { get; set; }
        public DbSet<Marriage> Marriages { get; set; }
        public DbSet<MarriageProposal> MarriageProposals { get; set; }
        public DbSet<PublicMessage> PublicMessages { get; set; }
        public DbSet<EstateOwnership> EstateOwnerships { get; set; }
        public DbSet<ParentChildRelationship> ParentChildRelationships { get; set; }
        public DbSet<CouncilMembership> CouncilMemberships { get; set; }
        public DbSet<Gene> Genes { get; set; }
        public DbSet<Castle> Castles { get; set; }
        public DbSet<NameObject> NameObjects { get; set; }
        public DbSet<WordAdjective> WordAdjectives { get; set; }
        public DbSet<WordAdverb> WordAdverbs { get; set; }
        public DbSet<WordNoun> WordNouns { get; set; }
        public DbSet<WordVerb> WordVerbs { get; set; }




        public GameState getGameState()
        {
            var gs = TheGameState.FirstOrDefault();
            if (gs == null)
            {
                gs = new GameState();
                TheGameState.Add(gs);
                gs.Month = 1;
                gs.Year = 0;
                SaveChanges();
            }
            return gs;
        }
    }
}
