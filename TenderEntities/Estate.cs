﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class Estate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Size { get; set; }
        public double Wealth { get; set; }
        /// <summary>
        /// used to contain the < 1.0 uncollectable part of taxes between months
        /// </summary>
        public double TaxFraction { get; set; }

        public override string ToString()
        {
            return Name + " " + (int)Size + " km², wealth: " + Math.Round(Wealth, 2);
        }
    }
}
