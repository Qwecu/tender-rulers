﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class CompetitionSkillRequirement
    {
        public int Id { get; set; }
        public int CompetitionId { get; set; }
        public int SkillId { get; set; }
        public double Weight { get; set; }
        public int? MinimumRequirement { get; set; }
    }
}
