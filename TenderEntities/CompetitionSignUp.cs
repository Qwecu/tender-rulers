﻿namespace TenderEntities
{
    public class CompetitionSignUp
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int CompetitionId { get; set; }
        public virtual Competition FK_Competition { get; set; }
    }
}