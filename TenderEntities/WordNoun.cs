﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class WordNoun
    {
        public int Id { get; set; }
        public string Word { get; set; }
    }
}
