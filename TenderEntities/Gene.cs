﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class Gene
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int GeneTypeId { get; set; }
        public byte Gene1 { get; set; }
        public byte Gene1Dominance { get; set; }
        public byte Gene2 { get; set; }
        public byte Gene2Dominance { get; set; }

        public byte GeneValue => Gene1Dominance >= Gene2Dominance ? Gene1 : Gene2;
    }
}
