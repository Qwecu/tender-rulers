﻿

namespace TenderEntities
{
    public class Dynasty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double GloryUnroundedValue { get; set; }
        public int Wealth { get; set; }
        public double WorkLeftBeforeNextCompetitionCanBePlanned { get; set; }
        public int? CastleId { get; set; }
        public int? PlayerId { get; set; }
        public virtual Player FK_Player { get; set; }

    }
}
