﻿namespace TenderEntities
{
    public class MarriageProposal
    {
        public int Id { get; set; }
        public int Dowry { get; set; }
        public int ProposerId { get; set; }
        public int ProposeeId { get; set; }
        public bool Active { get; set; }
    }
}