﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderRulers;

namespace TenderEntities
{
    public class EntityFactory
    {
        public static Dynasty RandomDynasty()
        {
            Dynasty dynasty = new Dynasty()
            {
                Name = TenderRulers.NameObject.randomNameString(),
                WorkLeftBeforeNextCompetitionCanBePlanned = GlobalSettings.workNeededPerCompetition,
                GloryUnroundedValue = 0,
                Wealth = 0
            };

            return dynasty;
        }
        public static Person RandomPerson()
        {
            Random random = GlobalSettings.random;
            Person person = new Person()
            {
                //Name = TenderRulers.NameObject.randomNameString(),
                GenderId = random.Next(2),
                FertilityModifier = random.NextDouble(),
                WordAdjectiveId = GlobalSettings.RandomAdjectiveId,
                WordAdverbId = GlobalSettings.RandomAdverbId,
                WordNounId = GlobalSettings.RandomNounId,
                WordVerbId = GlobalSettings.RandomVerbId
        };
            return person;
        }
        public static NameObject RandomNameObject(string[] namearray1 = null, string[] namearray2 = null, double[] nameLengthProbabilities = null)
        {
            NameObject name = new NameObject();

            if (namearray1 == null)
            {
                namearray1 = TenderRulers.NameObject.NameArray(4);

            }
            if (namearray2 == null)
            {
                namearray2 = TenderRulers.NameObject.NameArray(4);

            }

            name.DomCons1_1 = namearray1[0];
            name.DomVow1_2 = namearray1[1];
            name.DomCons1_3 = namearray1[2];
            name.DomCons2_1 = namearray1[3];
            name.DomVow2_2 = namearray1[4];
            name.DomCons2_3 = namearray1[5];
            name.DomCons3_1 = namearray1[6];
            name.DomVow3_2 = namearray1[7];
            name.DomCons3_3 = namearray1[8];
            name.DomCons4_1 = namearray1[9];
            name.DomVow4_2 = namearray1[10];
            name.DomCons4_3 = namearray1[11];

            name.RecCons1_1 = namearray2[0];
            name.RecVow1_2 = namearray2[1];
            name.RecCons1_3 = namearray2[2];
            name.RecCons2_1 = namearray2[3];
            name.RecVow2_2 = namearray2[4];
            name.RecCons2_3 = namearray2[5];
            name.RecCons3_1 = namearray2[6];
            name.RecVow3_2 = namearray2[7];
            name.RecCons3_3 = namearray2[8];
            name.RecCons4_1 = namearray2[9];
            name.RecVow4_2 = namearray2[10];
            name.RecCons4_3 = namearray2[11];

            name.longNameProbability1 = nameLengthProbabilities?[0] ?? GlobalSettings.random.NextDouble();
            name.longNameProbability2 = nameLengthProbabilities?[1] ?? GlobalSettings.random.NextDouble();
            name.longNameProbability3 = nameLengthProbabilities?[2] ?? GlobalSettings.random.NextDouble();
            name.longNameProbability4 = nameLengthProbabilities?[3] ?? GlobalSettings.random.NextDouble();

            var sum = name.longNameProbability1 + name.longNameProbability2 + name.longNameProbability3 + name.longNameProbability4;
            if (sum < GlobalSettings.ShortNameLimit)
            {
                name.SyllableCount = 2;
            }
            else if (sum > GlobalSettings.LongNameLimit)
            {
                name.SyllableCount = 4;
            }
            else
            {
                name.SyllableCount = 3;
            }

            string namestring = name.DomCons2_1 + name.DomVow2_2 + name.DomCons2_3 +
                                 name.DomCons3_1 + name.DomVow3_2 + name.DomCons3_3;
            if (name.SyllableCount > 2)
            {
                namestring = name.DomCons1_1 + name.DomVow1_2 + name.DomCons1_3 + namestring;
            }
            if (name.SyllableCount > 3)
            {
                namestring += name.DomCons4_1 + name.DomVow4_2 + name.DomCons4_3;
            }

            name.Name = namestring.ToString().Substring(0, 1).ToUpper() + namestring.ToString().Substring(1);

            return name;
        }

        public static NameObject RandomNameObject(NameObject mother, NameObject father)
        {
            var chromosome1 = mother != null ? RandomChildNameArray(mother) : TenderRulers.NameObject.NameArray(4);
            var chromosome2 = father != null ? RandomChildNameArray(father) : TenderRulers.NameObject.NameArray(4);
            for (int i = 0; i < chromosome1.Length; i++)
            {
                if (GlobalSettings.random.Next(2) == 0)
                {
                    var temp = chromosome1[i];
                    chromosome1[i] = chromosome2[i];
                    chromosome2[i] = temp;
                }
            }
            var longNamePorobabilities = new double[4];
            if (mother != null)
            {

                switch (GlobalSettings.random.Next(6))
                {
                    case 0:
                        longNamePorobabilities[0] = mother.longNameProbability1;
                        longNamePorobabilities[1] = mother.longNameProbability2;
                        break;
                    case 1:
                        longNamePorobabilities[0] = mother.longNameProbability1;
                        longNamePorobabilities[1] = mother.longNameProbability3;
                        break;
                    case 2:
                        longNamePorobabilities[0] = mother.longNameProbability1;
                        longNamePorobabilities[1] = mother.longNameProbability4;
                        break;
                    case 3:
                        longNamePorobabilities[0] = mother.longNameProbability2;
                        longNamePorobabilities[1] = mother.longNameProbability3;
                        break;
                    case 4:
                        longNamePorobabilities[0] = mother.longNameProbability2;
                        longNamePorobabilities[1] = mother.longNameProbability4;
                        break;
                    case 5:
                        longNamePorobabilities[0] = mother.longNameProbability3;
                        longNamePorobabilities[1] = mother.longNameProbability4;
                        break;
                }
            }
            else
            {
                longNamePorobabilities[0] = GlobalSettings.random.NextDouble();
                longNamePorobabilities[1] = GlobalSettings.random.NextDouble();

            }
            if (father != null)
            {
                switch (GlobalSettings.random.Next(6))
                {
                    case 0:
                        longNamePorobabilities[2] = father.longNameProbability1;
                        longNamePorobabilities[3] = father.longNameProbability2;
                        break;
                    case 1:
                        longNamePorobabilities[2] = father.longNameProbability1;
                        longNamePorobabilities[3] = father.longNameProbability3;
                        break;
                    case 2:
                        longNamePorobabilities[2] = father.longNameProbability1;
                        longNamePorobabilities[3] = father.longNameProbability4;
                        break;
                    case 3:
                        longNamePorobabilities[2] = father.longNameProbability2;
                        longNamePorobabilities[3] = father.longNameProbability3;
                        break;
                    case 4:
                        longNamePorobabilities[2] = father.longNameProbability2;
                        longNamePorobabilities[3] = father.longNameProbability4;
                        break;
                    case 5:
                        longNamePorobabilities[2] = father.longNameProbability3;
                        longNamePorobabilities[3] = father.longNameProbability4;
                        break;
                }
            }
            else
            {
                longNamePorobabilities[2] = GlobalSettings.random.NextDouble();
                longNamePorobabilities[3] = GlobalSettings.random.NextDouble();
            }

            return RandomNameObject(namearray1: chromosome1, namearray2: chromosome2, nameLengthProbabilities: longNamePorobabilities);
        }

        private static string[] RandomChildNameArray(NameObject parent)
        {
            var nameArray = new string[12];
            nameArray[0] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons1_1 : parent.RecCons1_1;
            nameArray[1] = GlobalSettings.random.Next(2) == 0 ? parent.DomVow1_2 : parent.RecVow1_2;
            nameArray[2] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons1_3 : parent.RecCons1_3;
            nameArray[3] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons2_1 : parent.RecCons2_1;
            nameArray[4] = GlobalSettings.random.Next(2) == 0 ? parent.DomVow2_2 : parent.RecVow2_2;
            nameArray[5] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons2_3 : parent.RecCons2_3;
            nameArray[6] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons3_1 : parent.RecCons3_1;
            nameArray[7] = GlobalSettings.random.Next(2) == 0 ? parent.DomVow3_2 : parent.RecVow3_2;
            nameArray[8] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons3_3 : parent.RecCons3_3;
            nameArray[9] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons4_1 : parent.RecCons4_1;
            nameArray[10] = GlobalSettings.random.Next(2) == 0 ? parent.DomVow4_2 : parent.RecVow4_2;
            nameArray[11] = GlobalSettings.random.Next(2) == 0 ? parent.DomCons4_3 : parent.RecCons4_3;
            return nameArray;
        }

        public static PersonalSkill RandomPersonalSkill()
        {
            Random random = GlobalSettings.random;
            PersonalSkill personalSkill = new PersonalSkill()
            {
                Gene1 = random.NextDouble() * GlobalSettings.DefaultMaxSkill * 1.0,
                Gene2 = random.NextDouble() * GlobalSettings.DefaultMaxSkill * 1.0
            };
            personalSkill.SkillPotential = (personalSkill.Gene1 + personalSkill.Gene2) / 2;
            return personalSkill;
        }
        public static PersonalSkill InheritedSkill(PersonalSkill mother, int babyId = 0)
        {
            PersonalSkill father = RandomPersonalSkill();
            father.SkillId = mother.SkillId;
            return InheritedSkill(mother, father, babyId);
        }

        /// <summary>
        /// Returns a new skill, inheriting half of the genes from the mother and half from the father
        /// </summary>
        /// <param name="mother">The mother skill</param>
        /// <param name="father">The father skill</param>
        public static PersonalSkill InheritedSkill(PersonalSkill mother, PersonalSkill father, int babyId = 0)
        {
            if (mother.SkillId != father.SkillId)
            {
                throw new ArgumentException("Non-matching skills");
            }
            var child = new PersonalSkill();
            Random random = GlobalSettings.random;
            child.SkillId = mother.SkillId;
            child.Gene1 = random.Next(2) == 0 ? mother.Gene1 : mother.Gene2;
            child.Gene2 = random.Next(2) == 0 ? father.Gene1 : father.Gene2;
            child.SkillPotential = (child.Gene1 + child.Gene2) / 2;
            child.PersonId = babyId;
            return child;
        }

        public static Estate RandomEstate()
        {
            return new Estate()
            {
                Name = TenderRulers.NameObject.randomNameString(),
                Size = GlobalSettings.randomEstateSize(),
                Wealth = GlobalSettings.randomEstateWealth()
            };
        }
        public static Gene RandomGene(int personId, int geneTypeId)
        {
            var gene = new Gene();
            var random = GlobalSettings.random;
            gene.PersonId = personId;
            gene.GeneTypeId = geneTypeId;

            var bytes = new byte[4];
            random.NextBytes(bytes);
            gene.Gene1 = bytes[0];
            gene.Gene1Dominance = bytes[1];
            gene.Gene2 = bytes[2];
            gene.Gene2Dominance = bytes[3];

            return gene;
        }
        public static Gene InheritedGene(int personId, Gene gene1, Gene gene2)
        {
            if (gene1.GeneTypeId != gene2.GeneTypeId)
            {
                throw new ArgumentException("Inherited genes must be of the same type");
            }
            var gene = new Gene();
            var random = GlobalSettings.random;

            gene.PersonId = personId;
            gene.GeneTypeId = gene1.GeneTypeId;

            int first = random.Next(2);
            int second = random.Next(2);

            gene.Gene1 = first == 0 ? gene1.Gene1 : gene1.Gene2;
            gene.Gene1Dominance = first == 0 ? gene1.Gene1Dominance : gene1.Gene2Dominance;

            gene.Gene1 = second == 0 ? gene2.Gene1 : gene2.Gene2;
            gene.Gene1Dominance = second == 0 ? gene2.Gene1Dominance : gene2.Gene2Dominance;

            return gene;
        }
    }
}
