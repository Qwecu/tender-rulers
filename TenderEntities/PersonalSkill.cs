﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class PersonalSkill
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int SkillId { get; set; }
        public double SkillValue { get; set; }
        public double SkillPotential { get; set; }
        public double Gene1 { get; set; }
        public double Gene2 { get; set; }
        public virtual Person FK_Person { get; set; }
        //public virtual Skill FK_Skill { get; set; }
    }
}
