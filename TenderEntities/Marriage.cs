﻿namespace TenderEntities
{
    public class Marriage
    {
        public int Id { get; set; }
        public int HusbandId { get; set; }
        public int WifeId { get; set; }
        public int MarriageYear { get; set; }
        public int MarriageMonth { get; set; }
        public bool Active { get; set; }
    }
}