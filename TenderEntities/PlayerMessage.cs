﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class PlayerMessage
    {
        public int Id { get; set; }
        public int PlayerId { get; set; }
        public int TypeId { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
    }
}
