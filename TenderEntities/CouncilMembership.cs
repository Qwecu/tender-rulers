﻿namespace TenderEntities
{
    public class CouncilMembership
    {
        public int Id { get; set; }
        public int DynastyId { get; set; }
        public int PersonId { get; set; }
        public int ChairId { get; set; }
    }
}