﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace TenderEntities
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            using (var db = new TenderContext())
            {
                db.Persons.Add(new Person { Name = "Another Person" });
                db.SaveChanges();

                foreach (var blog in db.Persons)
                {
                    Console.WriteLine(blog.Name, blog.PersonId);
                }
            }
            Console.ReadKey();
        }
    }
}
