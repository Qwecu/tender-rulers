﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TenderRulers;

namespace TenderEntities
{
    public class GameState
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Time => Year * 12 + Month;
        public Date Date =>  new Date(Year, Month); 
    }
}
