﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class Player
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<Dynasty> Dynasties { get;  set;}
    }
}
