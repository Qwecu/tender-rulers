﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class Skill
    {
        public int Id { get; set; }
        public int Name { get; set; }
    }
}
