﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class PublicMessage
    {
        public int Id { get; set; }
        public int Time { get; set; }
        public string Message { get; set; }
    }
}
