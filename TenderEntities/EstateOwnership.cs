﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class EstateOwnership
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public int EstateId { get; set; }
        //[Required]
        //public virtual Person Owner { get; set; }
        //[Required]
        //public virtual Estate Estate { get; set; }
    }
}
