﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class ParentChildRelationship
    {
        public int Id { get; set; }
        public int ChildId { get; set; }
        public int ParentId { get; set; }
        public int Type { get; set; }
        public int StartingTime { get; set; }
    }
}
