﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderEntities
{
    public class Competition
    {
        public int Id { get; set; }
        [ForeignKey("FK_Holder")]
        public int HolderId { get; set; }
        public string Name { get; set; }
        public int PrizeMoney { get; set; }
        public int YearMonthOfOccurrence { get; set; }
        public bool Executed { get; set; }
        [ForeignKey("FK_Winner")]
        public int? WinnerId { get; set; }
        public virtual Person FK_Holder { get; set; }
        public virtual Person FK_Winner { get; set; }
        public int Luck { get; set; }
        public double WinningResult { get; set; }
    }
}
