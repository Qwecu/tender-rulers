# README #


### What is this repository for? ###

This is a hobby project in C#/XAML that I'm doing for fun. It is a turn-based multiplayer online game about competing royal families, inspired by The Sims and Crusader Kings :)

*Namegen* contains a name generator

*Persons* contains the game logic

*GameUI* is an offline UI for the game, so it can be played without a server & client

*Networking* contains the server/client communication logic

*ServerApplication* is the server side application

*ClientApplication* is the client side application

...and *ClientUI* is a UI for that.