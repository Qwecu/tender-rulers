﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Persons;
using System.Collections.ObjectModel;

namespace GameUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    { 
        static GameStateRepository gameState;

        internal static ObservableCollection<PersonRepository> localLivingPeopleList;
        internal static ObservableCollection<string> messages;
        internal static ObservableCollection<CompetitionRepository> upcomingCompetitions;

        internal static PlayerRepository activePlayer;

        public static int year
        {
            get { return gameState.date.Year; }
        }

        public static int month
        {
            get {  return gameState.date.Month; }
        }

        internal static PlayerRepository getPlayer(string name)
        {
            return gameState.getPlayer(name);
        }

        internal static IList<PersonRepository> getPlayerCharacters(PlayerRepository activePlayer)
        {
            return gameState.getLivingPeople(activePlayer);
        }

        internal static IList<DynastyRepository> dynasties()
        {
            return gameState.getDynasties();
        }
        internal static IList<EstateRepository> estates()
        {
            return gameState.getEstates();
        }

        public static void advanceTimeOneMonth()
        {
            gameState.advanceTimeOneMonth();
            refreshLivingPeopleList();
            refreshCompetitionList();
            refreshMessages();
        }

        private static void refreshMessages()
        {
            messages.Clear();
            foreach (string s in gameState.getMessages())
            {
                messages.Add(s);
            }
        }

        private static void refreshLivingPeopleList()
        {
            var tempList = gameState.getLivingPeople();
            localLivingPeopleList.Clear();
            foreach(PersonRepository p in tempList)
            {
                localLivingPeopleList.Add(p);
            }
        }

        private static void refreshCompetitionList()
        {
            var tempList = gameState.getUpcomingCompetitions();
            upcomingCompetitions.Clear();
            foreach (CompetitionRepository c in tempList)
            {
                upcomingCompetitions.Add(c);
            }
        }

        static App()
        {
            gameState = new GameStateRepository();
            initializeApp();
        }

        private static void initializeApp()
        {
            localLivingPeopleList = new ObservableCollection<PersonRepository>();
            foreach (PersonRepository p in gameState.getLivingPeople())
            {
                localLivingPeopleList.Add(p);
            }
            upcomingCompetitions = new ObservableCollection<CompetitionRepository>();
            foreach (CompetitionRepository c in gameState.getUpcomingCompetitions())
            {
                upcomingCompetitions.Add(c);
            }
            messages = new ObservableCollection<string>();
            foreach (string s in gameState.getMessages())
            {
                messages.Add(s);
            }
        }

        internal static void setGameState(GameStateRepository gs)
        {
            gameState = gs;
            initializeApp();
        }

        internal static bool addMarriageProposal(PersonRepository male, PersonRepository female, int dowry)
        {
            return gameState.addMarriageProposal(male, female, dowry);
        }

        internal static void addPerson()
        {
            PersonRepository p = gameState.addNewRandomPerson();
            localLivingPeopleList.Add(p);
        }

        internal static void addNewPC()
        {
            if (activePlayer == null) return;
            PersonRepository p = gameState.addNewRandomPerson(activePlayer);
            localLivingPeopleList.Add(p);
        }

        internal static void addRandomEstate()
        {
            gameState.addRandomEstate();
            refreshLivingPeopleList();
        }

        internal static void addNewPlayer(string name)
        {
            gameState.addPlayer(name);
        }   
    }
}
