﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Persons;

namespace GameUI
{
    /// <summary>
    /// Interaction logic for DynastyView.xaml
    /// </summary>
    public partial class DynastyView : Page
    {
        public DynastyView()
        {
            InitializeComponent();
            DynastyList2.ItemsSource = App.dynasties();
        }

        private void DynastyList2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DynastyList2.SelectedItem != null)
            {
                DynastyRepository d = (DynastyRepository)DynastyList2.SelectedItem;
                DetailsList.ItemsSource = d.getVerbalInfo();
            }
        }
    }
}
