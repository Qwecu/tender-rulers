﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Persons;
using System.Collections.ObjectModel;

namespace GameUI
{
    /// <summary>
    /// Interaction logic for PlayerCharactersView.xaml
    /// </summary>
    public partial class PlayerCharactersView : Page
    {
        private PlayerRepository activePlayer;
        private IList<PersonRepository> playerCharacters;

        public PlayerCharactersView()
        {
            InitializeComponent();
        }

        public PlayerCharactersView(PlayerRepository activePlayer)
            :this()
        {
            this.activePlayer = activePlayer;            
            refreshView();
        }

        private void new_PC(object sender, RoutedEventArgs e)
        {            
            App.addNewPC();
            refreshView();
        }

        private void refreshView()
        {
            PlayerName.Text = activePlayer.playerName;
            playerCharacters = App.getPlayerCharacters(activePlayer);
            AllLivingPCs.ItemsSource = playerCharacters;
            AllLivingPersons.ItemsSource = App.localLivingPeopleList;
        }

        private void playerCharacters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            PersonRepository person = (PersonRepository)AllLivingPCs.SelectedItem;
            sb.Append(person.Name + "\n");
            sb.Append("wealth: " + person.wealth + "\n");
            PersonRepository spouse = person.Spouse();
            if (spouse != null)
            {
                sb.Append("Spouse: " + spouse.Name);
            }
            else
            {
                sb.Append(person.MaritalStatusString + "\n");
            }
            InfoBox.Text = sb.ToString();
        }

        private void PCs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void new_Proposal(object sender, RoutedEventArgs e)
        {
            PersonRepository male = AllLivingPCs.SelectedItem as PersonRepository;
            PersonRepository female = AllLivingPersons.SelectedItem as PersonRepository;
            if (male == null || female == null)
            {
                MessageBox.Show("You have to select a person from both lists first");
                return;                
            }
            if (male.genderString != "male" || female.genderString != "female")
            {
                MessageBox.Show("Select a male from the left list and a female from the right");
                return;
            }
            if (male.MaritalStatusString == "married" || female.MaritalStatusString == "married")
            {
                MessageBox.Show("Select two people that are not currently married");
                return;
            }
            int dowry = 0;
            var inputDialog = new InputDialog("Enter amout of dowry:");
            if (inputDialog.ShowDialog() == true)
            {
                string amount = inputDialog.Answer;
                bool d = int.TryParse(amount, out dowry);
            }
            App.addMarriageProposal(male, female, dowry);
        }

        private void viewMessages_Click(object sender, RoutedEventArgs e)
        {
            //TODO
        }
    }
}
