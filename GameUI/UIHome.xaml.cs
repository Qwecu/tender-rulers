﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Persons;
using System.Collections.ObjectModel;

namespace GameUI
{
    /// <summary>
    /// Interaction logic for UIHome.xaml
    /// </summary>
    public partial class UIHome : Page
    {
        private ObservableCollection<PersonRepository> people;
        public UIHome()
        {
            InitializeComponent();
            people = App.localLivingPeopleList;
            AllLivingPersons.ItemsSource = people;
            updateDisplayedDate();
            messageBox.ItemsSource = App.messages;
        }

        public UIHome(GameStateRepository gs)
        {
            InitializeComponent();
        }

        private void updateDisplayedDate()
        {
            Date.Text = String.Format("Month {0} of year {1}", App.month, App.year);
        }

        private void AllLivingPersons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            App.addPerson();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            object o = AllLivingPersons.SelectedItem;
            App.advanceTimeOneMonth();
            updateDisplayedDate();
            AllLivingPersons.SelectedIndex = AllLivingPersons.Items.IndexOf(o);
            messageScroller.ScrollToBottom();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (AllLivingPersons.SelectedIndex != -1)
            {
                var person = AllLivingPersons.SelectedItem as PersonRepository;
                MessageBox.Show(person.AgeString);
            }
        }

        private void testButtonClick(object sender, RoutedEventArgs e)
        {
            int index1 = AllLivingPersons.SelectedIndex;
            App.addRandomEstate();
            AllLivingPersons.SelectedIndex = index1;
        }


        private void detailsButtonClick(object sender, RoutedEventArgs e)
        {
            if (AllLivingPersons.SelectedItem == null)
            {
                MessageBox.Show("Please select a person first");
            }
            else
            {
                NavigationService.Navigate(new PersonDetails(AllLivingPersons.SelectedItem));
            }
        }

        private void Dynasties_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new DynastyView());
        }

        private void Estates_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new EstateView());
        }

        private void Competitions_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new CompetitionsView());
        }

        private void NewPlayer_Click(object sender, RoutedEventArgs e)
        {
            string name = "";
            var inputDialog = new InputDialog("Please enter your name:");
            if (inputDialog.ShowDialog() == true)
            {
                name = inputDialog.Answer;
            }
            if (name != "")
            {
                PlayerRepository player = App.getPlayer(name);
                if (player != null)
                {
                    App.activePlayer = player;
                }
                else
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("A player of this name does not exist. Do you want to create a new player?", "Player not found", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        App.addNewPlayer(name);
                        player = App.activePlayer = App.getPlayer(name);
                    }
                }
                if (player != null)
                {
                    PlayerName.Text = ("Current player: " + App.activePlayer.playerName);
                }   
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void NewPC_Click(object sender, RoutedEventArgs e)
        {
            App.addNewPC();
        }

        private void All_people_click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AllPeopleView());
        }

        private void year_button_Click(object sender, RoutedEventArgs e)
        {
            for(int i = 0; i < 12; i++)
            {
                button1_Click(sender, e);
            }
        }

        private void PCView_Click(object sender, RoutedEventArgs e)
        {
            if (App.activePlayer != null)
            {
                NavigationService.Navigate(new PlayerCharactersView(App.activePlayer));
            }
        }
    }
}
