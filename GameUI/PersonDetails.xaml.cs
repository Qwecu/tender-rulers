﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameUI
{
    /// <summary>
    /// Interaction logic for PersonDetails.xaml
    /// </summary>
    public partial class PersonDetails : Page
    {

        private PersonDetails()
        {
            InitializeComponent();
        }

        public PersonDetails(object selectedItem) : this()
        {
            this.DataContext = selectedItem;
            var fieldValues = selectedItem.GetType()
                     .GetFields(System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.NonPublic|System.Reflection.BindingFlags.Public)
                     .Select(field => (field.Name + " " + field.GetValue(selectedItem)))
                     .ToList();
            listBox.ItemsSource = fieldValues;
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
