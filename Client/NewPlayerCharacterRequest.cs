﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApplication
{
    [Serializable]
    public class NewPlayerCharacterRequest
    {
        public string PlayerName { get; private set; }
        public NewPlayerCharacterRequest(string playerName)
        {
            if (playerName == null)
            {
                throw new ArgumentException("Player name cannot be null");
            }
            this.PlayerName = playerName;
        }
    }
}
