﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApplication
{
    [Serializable]
    public class LoginRequest
    {
        public string Username { get; private set; }
        public string unsafePassword { get; private set; }

        public LoginRequest(string username, string unsafePassword)
        {
            this.Username = username;
            this.unsafePassword = unsafePassword;
        }
    }
}
