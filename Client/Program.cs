﻿using System;
using Persons;
using Networking;
using TenderRulers;

namespace ClientApplication
{
    public class Program
    {
        private GameStateRepository gameState;
        private string playerName;

        private string ipaddress;
        private int port;

        private GameDataSerializer gameDataSerializer;
        private Client client;

        public event Action<object, GameStateRefreshedEventArgs> GameStateRefreshed;
        public event Action<object, StringReceivedEventArgs> StringReceived;

        public Program()
        {
            ipaddress = "127.0.0.1";
            port = 9369;
            client = new Client(ipaddress, port);
            client.Connect();
            client.DataReceived += MessageReceived;

            gameDataSerializer = new GameDataSerializer();
        }

        public void AddNewPlayerCharacter()
        {
            NewPlayerCharacterRequest request = new NewPlayerCharacterRequest(playerName);
            sendToServer(request);
        }

        private void OnGameStateRefreshed()
        {
            var handler = GameStateRefreshed;
            if (handler != null)
            {
                GameStateRefreshed(this, new GameStateRefreshedEventArgs(gameState));
            }            
        }

        public static void Main()
        {
            Program p = new Program();
            Console.Read();
        }

        private void MessageReceived(object sender, DataReceivedEventArgs e)
        {
            object o = gameDataSerializer.ByteArrayToObject(e.data);
            GameStateRepository gs = o as GameStateRepository;
            if (gs != null)
            {
                gameState = gs;
                OnGameStateRefreshed();
                return;
            }
            string messageToDisplay = o as string;
            if(messageToDisplay != null)
            {
                OnStringReceived(messageToDisplay);
                return;
            }
        }

        private void OnStringReceived(string message)
        {
            var handler = StringReceived;
            if (handler != null)
            {
                StringReceived(this, new StringReceivedEventArgs(message));
            }
        }

        public GameStateRepository getGameState()
        {
            return gameState;
        }

        public void connectPlayer(string name, string password)
        {
            LoginRequest unsafeLoginRequest = new LoginRequest(name, password);
            sendToServer(unsafeLoginRequest);

            playerName = name;
        }

        private void sendToServer(object objectToSend)
        {
            byte[] data = gameDataSerializer.objectToByteArray(objectToSend);
            client.send(data);
        }
    }

    public class StringReceivedEventArgs : EventArgs
    {
        public string message { get; private set; }

        public StringReceivedEventArgs(string message)
        {
            this.message = message;
        }
    }

    public class GameStateRefreshedEventArgs : EventArgs
    {
        public GameStateRepository gameState { get; private set; }
        internal GameStateRefreshedEventArgs(GameStateRepository gs)
        {
            gameState = gs;
        }
    }
}
